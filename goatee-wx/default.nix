# This file is part of Goatee.
#
# Copyright 2014-2021 Bryan Gardiner
#
# Goatee is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Goatee is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

{ mkDerivation, base, goatee, lib, wx, wxcore }:
mkDerivation {
  pname = "goatee-wx";
  version = "0.4.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  enableSeparateDataOutput = true;
  executableHaskellDepends = [ base goatee wx wxcore ];
  homepage = "https://khumba.net/projects/goatee";
  description = "A monadic take on a 2,500-year-old board game - wxWidgets UI";
  license = lib.licenses.agpl3Plus;
}
