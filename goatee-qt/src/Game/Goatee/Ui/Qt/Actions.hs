-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

-- | Qt 'Action' definitions.
module Game.Goatee.Ui.Qt.Actions (
  Actions,
  create,
  destroy,
  myFileNew9Action,
  myFileNew13Action,
  myFileNew19Action,
  myFileNewCustomAction,
  myFileOpenAction,
  myFileSaveAction,
  myFileSaveAsAction,
  myFileCloseAction,
  myFileQuitAction,
  myEditCutNodeAction,
  myEditCopyNodeAction,
  myEditPasteNodeAction,
  myGamePassAction,
  myGameVariationsChildAction,
  myGameVariationsCurrentAction,
  myGameVariationsBoardMarkupOnAction,
  myGameVariationsBoardMarkupOffAction,
  myToolActions,
  myToolActionMap,
  myViewHighlightCurrentMovesAction,
  myViewStonesRegularModeAction,
  myViewStonesOneColorModeAction,
  myViewStonesBlindModeAction,
  myHelpKeyBindingsAction,
  myHelpAboutAction,
  ) where

import Control.Monad (forM, unless, void, when)
import qualified Data.Foldable as F
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Maybe (fromMaybe, isJust)
import Foreign.Hoppy.Runtime (withScopedPtr, toCppEnum)
import Game.Goatee.Ui.Qt.Common
import qualified Game.Goatee.Common.Bigfloat as BF
import Game.Goatee.Lib.Board
import Game.Goatee.Lib.Monad
import Game.Goatee.Lib.Property
import Game.Goatee.Lib.Types
import qualified Graphics.UI.Qtah.Gui.QDoubleValidator as QDoubleValidator
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Widgets.QAction as QAction
import qualified Graphics.UI.Qtah.Widgets.QActionGroup as QActionGroup
import qualified Graphics.UI.Qtah.Widgets.QBoxLayout as QBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QDialog as QDialog
import qualified Graphics.UI.Qtah.Widgets.QDialogButtonBox as QDialogButtonBox
import qualified Graphics.UI.Qtah.Widgets.QGridLayout as QGridLayout
import qualified Graphics.UI.Qtah.Widgets.QLabel as QLabel
import qualified Graphics.UI.Qtah.Widgets.QLineEdit as QLineEdit
import qualified Graphics.UI.Qtah.Widgets.QPushButton as QPushButton
import qualified Graphics.UI.Qtah.Widgets.QSpinBox as QSpinBox
import qualified Graphics.UI.Qtah.Widgets.QVBoxLayout as QVBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget

data Actions ui = Actions
  { myUi :: ui
  , myState :: ViewState
  , myFileNew9Action :: QAction.QAction
  , myFileNew13Action :: QAction.QAction
  , myFileNew19Action :: QAction.QAction
  , myFileNewCustomAction :: QAction.QAction
  , myFileOpenAction :: QAction.QAction
  , myFileSaveAction :: QAction.QAction
  , myFileSaveAsAction :: QAction.QAction
  , myFileCloseAction :: QAction.QAction
  , myFileQuitAction :: QAction.QAction
  , myEditCutNodeAction :: QAction.QAction
  , myEditCopyNodeAction :: QAction.QAction
  , myEditPasteNodeAction :: QAction.QAction
  , myGamePassAction :: QAction.QAction
  , myGameVariationsChildAction :: QAction.QAction
  , myGameVariationsCurrentAction :: QAction.QAction
  , myGameVariationsBoardMarkupOnAction :: QAction.QAction
  , myGameVariationsBoardMarkupOffAction :: QAction.QAction
  , myToolActions :: QActionGroup.QActionGroup
  , myToolActionMap :: Map ToolType QAction.QAction
  , myViewHighlightCurrentMovesAction :: QAction.QAction
  , myViewStonesRegularModeAction :: QAction.QAction
  , myViewStonesOneColorModeAction :: QAction.QAction
  , myViewStonesBlindModeAction :: QAction.QAction
  , myHelpKeyBindingsAction :: QAction.QAction
  , myHelpAboutAction :: QAction.QAction
  , myModesChangedHandler :: IORef (Maybe Registration)
  }

instance UiCtrl go ui => UiView go ui (Actions ui) where
  viewName = const "Actions"
  viewCtrl = myUi
  viewState = myState
  viewUpdate = update

create :: UiCtrl go ui => ui -> IO (Actions ui)
create ui = do
  let toolTypes = enumFrom minBound

  owner <- getQMainWindow "Actions.create" ui

  -- File actions.
  fileNew9Action <- QAction.newWithTextAndParent "New &9x9 board" owner
  connect_ fileNew9Action QAction.triggeredSignal $ \_ ->
    void $ openNewBoard (Just ui) (Just (9, 9))

  fileNew13Action <- QAction.newWithTextAndParent "New 1&3x13 board" owner
  connect_ fileNew13Action QAction.triggeredSignal $ \_ ->
    void $ openNewBoard (Just ui) (Just (13, 13))

  fileNew19Action <- QAction.newWithTextAndParent "New &19x19 board" owner
  connect_ fileNew19Action QAction.triggeredSignal $ \_ ->
    void $ openNewBoard (Just ui) (Just (19, 19))

  fileNewCustomAction <- QAction.newWithTextAndParent "New &custom board..." owner
  connect_ fileNewCustomAction QAction.triggeredSignal $ \_ ->
    withScopedPtr QDialog.new $ \dialog -> do
      QWidget.setWindowTitle dialog "New custom board"

      dialogBox <- QVBoxLayout.new
      controlsBox <- QGridLayout.new
      buttonBox <- QDialogButtonBox.new
      QWidget.setLayout dialog dialogBox
      QBoxLayout.addLayout dialogBox controlsBox
      QBoxLayout.addSpacing dialogBox 10
      QBoxLayout.addWidget dialogBox buttonBox

      -- SGF only supports boards up to 'boardSizeMax', but Goatee works fine with
      -- larger boards.  The spinner wants an upper bound, so let's at least set
      -- something that isn't too outrageous along a single dimension.
      let arbitraryUpperLimit = 1000
          makeSpinBox lower upper = do
            spin <- QSpinBox.new
            QSpinBox.setRange spin lower upper
            return spin
          makeBoardSizeSpinBox = makeSpinBox boardSizeMin arbitraryUpperLimit

      widthLabel <- QLabel.newWithText "&Width"
      widthSpin <- makeBoardSizeSpinBox
      QLabel.setBuddy widthLabel widthSpin
      QGridLayout.addWidget controlsBox widthLabel 0 0
      QGridLayout.addWidget controlsBox widthSpin 0 1

      heightLabel <- QLabel.newWithText "&Height"
      heightSpin <- makeBoardSizeSpinBox
      QLabel.setBuddy heightLabel heightSpin
      QGridLayout.addWidget controlsBox heightLabel 1 0
      QGridLayout.addWidget controlsBox heightSpin 1 1

      handicapLabel <- QLabel.newWithText "H&andicap"
      handicapSpin <- makeSpinBox 0 9
      QLabel.setBuddy handicapLabel handicapSpin
      QGridLayout.addWidget controlsBox handicapLabel 2 0
      QGridLayout.addWidget controlsBox handicapSpin 2 1

      komiLabel <- QLabel.newWithText "&Komi"
      komiEdit <- QLineEdit.new
      komiValidator <- QDoubleValidator.newWithOptions (-1000) 1000 1
      QLabel.setBuddy komiLabel komiEdit
      QLineEdit.setText komiEdit "0"
      QLineEdit.setValidator komiEdit komiValidator
      QGridLayout.addWidget controlsBox komiLabel 3 0
      QGridLayout.addWidget controlsBox komiEdit 3 1

      okButton <- QDialogButtonBox.addStandardButton buttonBox QDialogButtonBox.Ok
      QDialogButtonBox.addStandardButton buttonBox QDialogButtonBox.Cancel
      QPushButton.setDefault okButton True

      QSpinBox.setValue widthSpin boardSizeDefault
      QSpinBox.setValue heightSpin boardSizeDefault

      connect_ dialog QDialog.finishedSignal $ \response -> do
        width <- QSpinBox.value widthSpin
        height <- QSpinBox.value heightSpin
        handicap <- QSpinBox.value handicapSpin
        -- TODO Don't just 'read' here.
        komi <- BF.fromDouble . read <$> QLineEdit.text komiEdit
        when (toCppEnum (fromIntegral response) == QDialog.Accepted) $ do
          ui' <- openNewBoard (Just ui) (Just (width, height))
          when (komi /= 0) $ doUiGo ui' $ putProperty $ KM komi
          when (handicap > 0) $ do
            -- If the board size + handicap configuration is known, then set up the
            -- board for the handicap, otherwise, leave the user to do it.
            --
            -- TODO If the configuration is unknown and handicap >= 2, then maybe
            -- set HA at least, anyway?
            let stones = fromMaybe [] $ handicapStones width height handicap
            unless (null stones) $ do
              doUiGo ui' $ do
                putProperty $ HA handicap
                putProperty $ AB $ coords stones
                putProperty $ PL White
              setDirty ui' False

      QDialog.open dialog

  fileOpenAction <- QAction.newWithTextAndParent "&Open file..." owner
  connect_ fileOpenAction QAction.triggeredSignal $ const $ fileOpen ui

  fileSaveAction <- QAction.newWithTextAndParent "&Save file" owner
  connect_ fileSaveAction QAction.triggeredSignal $ const $ void $ fileSave ui

  fileSaveAsAction <- QAction.newWithTextAndParent "Sa&ve file as..." owner
  connect_ fileSaveAsAction QAction.triggeredSignal $ const $ void $ fileSaveAs ui

  fileCloseAction <- QAction.newWithTextAndParent "&Close" owner
  connect_ fileCloseAction QAction.triggeredSignal $ const $ void $ fileClose ui

  fileQuitAction <- QAction.newWithTextAndParent "&Quit" owner
  connect_ fileQuitAction QAction.triggeredSignal $ const $ void $ fileQuit ui

  -- Edit actions.
  editCutNodeAction <- QAction.newWithTextAndParent "Cut current node" owner
  connect_ editCutNodeAction QAction.triggeredSignal $ const $ editCutNode ui

  editCopyNodeAction <- QAction.newWithTextAndParent "Copy current node" owner
  connect_ editCopyNodeAction QAction.triggeredSignal $ const $ editCopyNode ui

  editPasteNodeAction <- QAction.newWithTextAndParent "Paste node as child" owner
  connect_ editPasteNodeAction QAction.triggeredSignal $ const $ editPasteNode ui

  -- Game actions.
  gamePassAction <- QAction.newWithTextAndParent "&Pass" owner
  connect_ gamePassAction QAction.triggeredSignal $ const $ playAt ui Nothing

  gameVariationsWhichActionGroup <- QActionGroup.new owner
  gameVariationsChildAction <-
    QActionGroup.addNewAction gameVariationsWhichActionGroup "&Child variations"
  -- TODO Use status tips instead?
  QAction.setCheckable gameVariationsChildAction True
  QAction.setToolTip gameVariationsChildAction "Show children node as variations"
  gameVariationsCurrentAction <-
    QActionGroup.addNewAction gameVariationsWhichActionGroup "C&urrent variations"
  QAction.setCheckable gameVariationsCurrentAction True
  QAction.setToolTip gameVariationsCurrentAction "Show variations of the current node"

  gameVariationsToggleActionGroup <- QActionGroup.new owner
  gameVariationsBoardMarkupOnAction <-
    QActionGroup.addNewAction gameVariationsToggleActionGroup "&Show on board"
  QAction.setCheckable gameVariationsBoardMarkupOnAction True
  QAction.setToolTip gameVariationsBoardMarkupOnAction "Show move variations on the board"
  gameVariationsBoardMarkupOffAction <-
    QActionGroup.addNewAction gameVariationsToggleActionGroup "&Hide on board"
  QAction.setCheckable gameVariationsBoardMarkupOffAction True
  QAction.setToolTip gameVariationsBoardMarkupOffAction "Hide move variations on the board"

  connect_ gameVariationsChildAction QAction.toggledSignal $ \checked -> when checked $
    doUiGo ui $ modifyVariationMode $ \mode -> mode { variationModeSource = ShowChildVariations }
  connect_ gameVariationsCurrentAction QAction.toggledSignal $ \checked -> when checked $
    doUiGo ui $ modifyVariationMode $ \mode -> mode { variationModeSource = ShowCurrentVariations }

  connect_ gameVariationsBoardMarkupOnAction QAction.toggledSignal $ \checked -> when checked $ do
    doUiGo ui $ modifyVariationMode $ \mode -> mode { variationModeBoardMarkup = True }
  connect_ gameVariationsBoardMarkupOffAction QAction.toggledSignal $ \checked -> when checked $ do
    doUiGo ui $ modifyVariationMode $ \mode -> mode { variationModeBoardMarkup = False }

  -- Tool actions.
  toolActions <- QActionGroup.new owner
  toolActionMap <- fmap Map.fromList $ forM toolTypes $ \toolType -> do
    AnyTool tool <- findTool ui toolType
    toolAction <- QActionGroup.addNewAction toolActions $ toolLabel tool
    QAction.setCheckable toolAction True
    connect_ toolAction QAction.toggledSignal $ \checked -> when checked $ setTool ui toolType
    return (toolType, toolAction)

  -- View actions.
  viewHighlightCurrentMovesAction <- QAction.newWithTextAndParent "Highlight &current moves" owner
  QAction.setCheckable viewHighlightCurrentMovesAction True
  connect_ viewHighlightCurrentMovesAction QAction.toggledSignal $ \checked ->
    modifyModes ui $ \modes -> return modes { uiHighlightCurrentMovesMode = checked }

  viewStonesActionGroup <- QActionGroup.new owner
  viewStonesRegularModeAction <- QActionGroup.addNewAction viewStonesActionGroup "&Regular"
  QAction.setCheckable viewStonesRegularModeAction True
  QAction.setToolTip viewStonesRegularModeAction "Regular Go: Render stones on the board normally."
  viewStonesOneColorModeAction <- QActionGroup.addNewAction viewStonesActionGroup "&One-color"
  QAction.setCheckable viewStonesOneColorModeAction True
  QAction.setToolTip viewStonesOneColorModeAction
    "One-color Go: Both players use the same color stones."
  viewStonesBlindModeAction <- QActionGroup.addNewAction viewStonesActionGroup "&Blind"
  QAction.setCheckable viewStonesBlindModeAction True
  QAction.setToolTip viewStonesBlindModeAction "Blind Go: No stones are visible on the board."

  connect_ viewStonesRegularModeAction QAction.toggledSignal $ \checked ->
    when checked $ modifyModes ui $ \modes ->
    return modes { uiViewStonesMode = ViewStonesRegularMode }
  connect_ viewStonesOneColorModeAction QAction.toggledSignal $ \checked ->
    when checked $ modifyModes ui $ \modes ->
    return modes { uiViewStonesMode = ViewStonesOneColorMode }
  connect_ viewStonesBlindModeAction QAction.toggledSignal $ \checked ->
    when checked $ modifyModes ui $ \modes ->
    return modes { uiViewStonesMode = ViewStonesBlindMode }

  -- Help actions.
  helpKeyBindingsAction <- QAction.newWithTextAndParent "&Key bindings" owner
  connect_ helpKeyBindingsAction QAction.triggeredSignal $ const $ helpKeyBindings ui

  helpAboutAction <- QAction.newWithTextAndParent "&About" owner
  connect_ helpAboutAction QAction.triggeredSignal $ const $ helpAbout ui

  -- TODO Don't need this now?
  --actionActivate =<<
  --  fmap (fromMaybe $ error $ "Could not find the initial tool " ++ show initialToolType ++ ".")
  --       (actionGroupGetAction toolActions $ show initialToolType)

  state <- viewStateNew
  modesChangedHandler <- newIORef Nothing

  let me = Actions
        { myUi = ui
        , myState = state
        , myFileNew9Action = fileNew9Action
        , myFileNew13Action = fileNew13Action
        , myFileNew19Action = fileNew19Action
        , myFileNewCustomAction = fileNewCustomAction
        , myFileOpenAction = fileOpenAction
        , myFileSaveAction = fileSaveAction
        , myFileSaveAsAction = fileSaveAsAction
        , myFileCloseAction = fileCloseAction
        , myFileQuitAction = fileQuitAction
        , myEditCutNodeAction = editCutNodeAction
        , myEditCopyNodeAction = editCopyNodeAction
        , myEditPasteNodeAction = editPasteNodeAction
        , myGamePassAction = gamePassAction
        , myGameVariationsChildAction = gameVariationsChildAction
        , myGameVariationsCurrentAction = gameVariationsCurrentAction
        , myGameVariationsBoardMarkupOnAction = gameVariationsBoardMarkupOnAction
        , myGameVariationsBoardMarkupOffAction = gameVariationsBoardMarkupOffAction
        , myToolActions = toolActions
        , myToolActionMap = toolActionMap
        , myViewHighlightCurrentMovesAction = viewHighlightCurrentMovesAction
        , myViewStonesRegularModeAction = viewStonesRegularModeAction
        , myViewStonesOneColorModeAction = viewStonesOneColorModeAction
        , myViewStonesBlindModeAction = viewStonesBlindModeAction
        , myHelpKeyBindingsAction = helpKeyBindingsAction
        , myHelpAboutAction = helpAboutAction
        , myModesChangedHandler = modesChangedHandler
        }

  initialize me
  return me

initialize :: UiCtrl go ui => Actions ui -> IO ()
initialize me = do
  let ui = myUi me
  register me
    [ AnyEvent navigationEvent
    , AnyEvent variationModeChangedEvent
    ]
  writeIORef (myModesChangedHandler me) =<<
    fmap Just (registerModesChangedHandler ui "Actions" $ \_ _ -> update me)
  update me

destroy :: UiCtrl go ui => Actions ui -> IO ()
destroy me = do
  let ui = myUi me
  F.mapM_ (unregisterModesChangedHandler ui) =<< readIORef (myModesChangedHandler me)
  viewDestroy me

update :: UiCtrl go ui => Actions ui -> IO ()
update me = do
  cursor <- readCursor $ myUi me
  modes <- readModes $ myUi me

  -- Update the sensitivity of the "Edit > Cut node" action.
  QAction.setEnabled (myEditCutNodeAction me) $ isJust $ cursorParent cursor

  QAction.setChecked (myViewHighlightCurrentMovesAction me) $
    uiHighlightCurrentMovesMode modes

  let viewStonesAction = case uiViewStonesMode modes of
        ViewStonesRegularMode -> myViewStonesRegularModeAction me
        ViewStonesOneColorMode -> myViewStonesOneColorModeAction me
        ViewStonesBlindMode -> myViewStonesBlindModeAction me
  QAction.setChecked viewStonesAction True

  updateVariationModeActions me cursor
  updateToolActions me

-- | Updates the selection variation mode radio actions.
updateVariationModeActions :: Actions ui -> Cursor -> IO ()
updateVariationModeActions me cursor = do
  let new = rootInfoVariationMode $ gameInfoRootInfo $ boardGameInfo $
            cursorBoard cursor
      newSource = variationModeSource new
      newBoardMarkup = variationModeBoardMarkup new
      sourceAction = case newSource of
        ShowChildVariations -> myGameVariationsChildAction me
        ShowCurrentVariations -> myGameVariationsCurrentAction me
      boardMarkupAction = case newBoardMarkup of
        True -> myGameVariationsBoardMarkupOnAction me
        False -> myGameVariationsBoardMarkupOffAction me

  QAction.setChecked sourceAction True
  QAction.setChecked boardMarkupAction True

-- | Updates the active tool radio action.
updateToolActions :: UiCtrl go ui => Actions ui -> IO ()
updateToolActions me = do
  let ui = myUi me
  toolType <- uiToolType <$> readModes ui
  case Map.lookup toolType $ myToolActionMap me of
    Just toolAction -> QAction.setChecked toolAction True
    Nothing ->
      fail $ concat
      ["updateToolActions: Couldn't find an action for tool ", show toolType, "."]
