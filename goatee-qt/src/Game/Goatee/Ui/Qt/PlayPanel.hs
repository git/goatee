-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

module Game.Goatee.Ui.Qt.PlayPanel (
  PlayPanel,
  create,
  destroy,
  myWidget,
  ) where

import Control.Concurrent.MVar (modifyMVar_, newEmptyMVar, putMVar)
import Control.Monad (forM, void, when)
import Data.Foldable (forM_, mapM_)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Maybe (catMaybes)
import qualified Data.Set as Set
import Game.Goatee.Common
import Game.Goatee.Lib.Board
import Game.Goatee.Lib.Monad (
  AnyEvent (..),
  goDown,
  goToRoot,
  goUp,
  modifyPropertyString,
  navigationEvent,
  propertiesModifiedEvent,
  )
import Game.Goatee.Lib.Property
import Game.Goatee.Lib.Tree
import Game.Goatee.Lib.Types
import Game.Goatee.Ui.Qt.Common
import qualified Graphics.UI.Qtah.Event as Event
import qualified Graphics.UI.Qtah.Gui.QShowEvent as QShowEvent
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Widgets.QAbstractButton as QAbstractButton
import qualified Graphics.UI.Qtah.Widgets.QBoxLayout as QBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QHBoxLayout as QHBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QPushButton as QPushButton
import qualified Graphics.UI.Qtah.Widgets.QTextEdit as QTextEdit
import qualified Graphics.UI.Qtah.Widgets.QVBoxLayout as QVBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget
import Prelude hiding (mapM_)

data PlayPanel ui = PlayPanel
  { myUi :: ui
  , myState :: ViewState
  , myWidget :: QWidget.QWidget
  , myComment :: QTextEdit.QTextEdit
  , myModesChangedHandler :: IORef (Maybe Registration)
  }

instance UiCtrl go ui => UiView go ui (PlayPanel ui) where
  viewName = const "PlayPanel"
  viewCtrl = myUi
  viewState = myState
  viewUpdate = update

create :: UiCtrl go ui => ui -> IO (PlayPanel ui)
create ui = do
  widget <- QWidget.new
  box <- QVBoxLayout.new
  QWidget.setLayout widget box

  navBox <- QHBoxLayout.new
  QBoxLayout.addLayout box navBox
  startButton <- QPushButton.newWithText "<<"
  prevButton <- QPushButton.newWithText "<"
  nextButton <- QPushButton.newWithText ">"
  endButton <- QPushButton.newWithText ">>"
  mapM_ (\b -> QBoxLayout.addWidgetWithStretch navBox b 1)
    [startButton, prevButton, nextButton, endButton]
  let onClick b f = connect_ b QAbstractButton.clickedSignal $ \_ -> f
  onClick startButton $ doUiGo ui goToRoot
  onClick prevButton $ doUiGo ui $ void goUp
  onClick nextButton $ doUiGo ui $ void $ goDown 0
  onClick endButton $ doUiGo ui $ whileM (goDown 0) $ return ()

  -- Add the widgets of all of the tools.  Deduplicate the widgets so those that
  -- are shared between tools only get added once; GTK+ doesn't like having a
  -- widget added multiple times.
  -- TODO(Qt) Check that this deduplication works with Qtah.
  toolWidgets <- fmap catMaybes $
                 forM [minBound..] $
                 fmap (\(AnyTool tool) -> toolPanelWidget tool) .
                 findTool ui
  forM_ (Set.toList $ Set.fromList toolWidgets) $ \widget ->
    QBoxLayout.addWidget box widget

  -- TODO(Qt) Check that scrollbars appear as QTextEdit promises they will.
  comment <- QTextEdit.new
  QTextEdit.setLineWrapMode comment QTextEdit.WidgetWidth
  QBoxLayout.addWidgetWithStretch box comment 1

  connect_ comment QTextEdit.textChangedSignal $ do
    newText <- QTextEdit.toPlainText comment
    doUiGo ui $ modifyPropertyString propertyC $ const newText

  state <- viewStateNew
  modesChangedHandler <- newIORef Nothing

  let me = PlayPanel
        { myUi = ui
        , myState = state
        , myWidget = widget
        , myComment = comment
        , myModesChangedHandler = modesChangedHandler
        }

  -- After the panel is shown, we only want the tool widget for the active tool
  -- to be visible.
  -- TODO(Qt) Is QShowEvent a suitable substitute for GTK+'s afterShow?
  initEventVar <- newEmptyMVar
  putMVar initEventVar =<<
    (fmap Just $ Event.onEvent (myWidget me) $ \(_ :: QShowEvent.QShowEvent) -> do
       updateVisibleToolWidget me
       modifyMVar_ initEventVar $ \maybeRegistration -> do
         mapM_ Event.unregister maybeRegistration
         return Nothing
       return False)

  initialize me
  return me

initialize :: UiCtrl go ui => PlayPanel ui -> IO ()
initialize me = do
  let ui = myUi me

  register me
    [ AnyEvent navigationEvent
    , AnyEvent propertiesModifiedEvent
    ]

  writeIORef (myModesChangedHandler me) =<<
    fmap Just (registerModesChangedHandler ui "PlayPanel" $ checkForToolChange me)

  viewUpdate me

destroy :: UiCtrl go ui => PlayPanel ui -> IO ()
destroy me = do
  let ui = myUi me
  mapM_ (unregisterModesChangedHandler ui) =<< readIORef (myModesChangedHandler me)
  viewDestroy me

update :: UiCtrl go ui => PlayPanel ui -> IO ()
update me = do
  cursor <- readCursor (myUi me)

  let comment = myComment me
      newComment = maybe "" fromText $ findPropertyValue propertyC $ cursorNode cursor
  oldComment <- QTextEdit.toPlainText comment
  when (newComment /= oldComment) $ QTextEdit.setPlainText comment newComment

-- | Updates the visibility of all tool widgets, hiding all widgets of inactive
-- tools and showing the widget of the active tool.
updateVisibleToolWidget :: UiCtrl go ui => PlayPanel ui -> IO ()
updateVisibleToolWidget me = do
  let ui = myUi me
  activeToolType <- (\(AnyTool tool) -> toolType tool) <$> readTool ui
  forM_ [minBound..] $ \toolType ->
    findTool ui toolType >>= \(AnyTool tool) ->
    forM_ (toolPanelWidget tool) $ \widget ->
    QWidget.setVisible widget $ toolType == activeToolType

-- | Checks for a change in active tool between the two modes; if one is found,
-- the deactivating tool's widget is hidden and the activating tool's widget is
-- shown.
checkForToolChange :: UiCtrl go ui => PlayPanel ui -> UiModes -> UiModes -> IO ()
checkForToolChange me oldModes newModes = do
  let ui = myUi me
      oldTool = uiToolType oldModes
      newTool = uiToolType newModes
  when (newTool /= oldTool) $ do
    findTool ui oldTool >>= \(AnyTool tool) -> mapM_ QWidget.hide $ toolPanelWidget tool
    findTool ui newTool >>= \(AnyTool tool) -> mapM_ QWidget.show $ toolPanelWidget tool
