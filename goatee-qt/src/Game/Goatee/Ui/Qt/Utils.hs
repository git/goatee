-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

-- | General Qt-related utilities.
module Game.Goatee.Ui.Qt.Utils (
  spinBoxGetValueAsBigfloat,
  ) where

import qualified Game.Goatee.Common.Bigfloat as BF
import qualified Graphics.UI.Qtah.Widgets.QDoubleSpinBox as QDoubleSpinBox

-- | Retrieves the current value of a spin box as a 'BF.Bigfloat' that's
-- rounded to the number of digits the spin box is configured for.
spinBoxGetValueAsBigfloat :: QDoubleSpinBox.QDoubleSpinBox -> IO BF.Bigfloat
spinBoxGetValueAsBigfloat = fmap BF.fromDouble . QDoubleSpinBox.value
