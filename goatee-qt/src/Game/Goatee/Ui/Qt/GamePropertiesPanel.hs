-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

-- | A panel that displays a vertical list of controls for editing a game's
-- 'GameInfo'.
module Game.Goatee.Ui.Qt.GamePropertiesPanel (
  GamePropertiesPanel,
  create,
  destroy,
  myWidget,
  ) where

import Control.Arrow (first)
import Control.Monad (forM_, void, when)
import Data.IORef (newIORef, readIORef, writeIORef)
import Data.Maybe (fromMaybe)
import qualified Game.Goatee.Common.Bigfloat as BF
import Game.Goatee.Lib.Board
import Game.Goatee.Lib.Monad hiding (on)
import Game.Goatee.Lib.Types
import Game.Goatee.Ui.Qt.Common
import Game.Goatee.Ui.Qt.Widget
import Graphics.UI.Qtah.Event (onEvent)
import qualified Graphics.UI.Qtah.Gui.QFocusEvent as QFocusEvent
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Widgets.QAbstractButton as QAbstractButton
import qualified Graphics.UI.Qtah.Widgets.QBoxLayout as QBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QCheckBox as QCheckBox
import qualified Graphics.UI.Qtah.Widgets.QDoubleSpinBox as QDoubleSpinBox
import qualified Graphics.UI.Qtah.Widgets.QFrame as QFrame
import qualified Graphics.UI.Qtah.Widgets.QGridLayout as QGridLayout
import qualified Graphics.UI.Qtah.Widgets.QHBoxLayout as QHBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QLabel as QLabel
import qualified Graphics.UI.Qtah.Widgets.QScrollArea as QScrollArea
import qualified Graphics.UI.Qtah.Widgets.QTextEdit as QTextEdit
import qualified Graphics.UI.Qtah.Widgets.QVBoxLayout as QVBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget

-- A getter for 'Stringlike' 'GameInfo' fields.
data InfoGetter = forall a. Stringlike a => InfoGetter (GameInfo -> Maybe a)

data GamePropertiesPanel ui = GamePropertiesPanel
  { myUi :: ui
  , myState :: ViewState
  , myWidget :: QWidget.QWidget

    -- Black's info.
  , myBlackNameEdit :: GoateeLineEdit
  , myBlackRankEdit :: GoateeLineEdit
  , myBlackTeamEdit :: GoateeLineEdit

    -- White's info.
  , myWhiteNameEdit :: GoateeLineEdit
  , myWhiteRankEdit :: GoateeLineEdit
  , myWhiteTeamEdit :: GoateeLineEdit

    -- Game rules.
  , myRulesetEdit :: GoateeLineEdit
  , myMainTimeSpin :: GoateeSpinBox
  , myMainTimeLabel :: QLabel.QLabel
  , myOvertimeEdit :: GoateeLineEdit
  , myGameResultDisplayCheck :: QCheckBox.QCheckBox
  , myGameResultEdit :: GoateeLineEdit

    -- Game editors.
  , myGameAnnotatorEdit :: GoateeLineEdit
  , myGameEntererEdit :: GoateeLineEdit

    -- Game context.
  , myEventNameEdit :: GoateeLineEdit
  , myGamePlaceEdit :: GoateeLineEdit
  , myGameRoundEdit :: GoateeLineEdit
  , myGameDatesEdit :: GoateeLineEdit
  , myGameNameEdit :: GoateeLineEdit
  , myGameSourceEdit :: GoateeLineEdit
  , myGameCopyrightEdit :: GoateeLineEdit

    -- Further commentary.
  , myGameOpeningEdit :: GoateeLineEdit
  , myGameCommentTextEdit :: QTextEdit.QTextEdit
  }

instance UiCtrl go ui => UiView go ui (GamePropertiesPanel ui) where
  viewName = const "GamePropertiesPanel"
  viewCtrl = myUi
  viewState = myState
  viewUpdate = update

create :: UiCtrl go ui => ui -> IO (GamePropertiesPanel ui)
create ui = do
  scroll <- QScrollArea.new
  scrollContents <- QWidget.new
  box <- QVBoxLayout.new
  grid <- QGridLayout.new
  QScrollArea.setWidgetResizable scroll True
  QScrollArea.setWidget scroll scrollContents
  QWidget.setLayout scrollContents box
  QBoxLayout.addLayout box grid

  nextRowRef <- newIORef 0
  let nextRow = do
        row <- readIORef nextRowRef
        writeIORef nextRowRef $ row + 1
        return row
      addSeparator = do
        row <- nextRow
        separator <- QFrame.new
        QFrame.setFrameShape separator QFrame.HLine
        QFrame.setFrameShadow separator QFrame.Sunken
        QWidget.setMinimumHeight separator 2
        QGridLayout.addWidgetWithSpan grid separator row 0 1 2
      addWidget :: QWidget.QWidgetPtr widget => String -> widget -> IO widget
      addWidget labelText widget = do
        row <- nextRow
        label <- QLabel.newWithText labelText
        QLabel.setBuddy label widget
        QGridLayout.addWidget grid label row 0
        QGridLayout.addWidget grid widget row 1
        return widget
      addWideWidget :: QWidget.QWidgetPtr widget => widget -> IO widget
      addWideWidget widget = do
        row <- nextRow
        QGridLayout.addWidgetWithSpan grid widget row 0 1 2
        return widget
      addLineEdit :: String -> IO GoateeLineEdit
      addLineEdit labelText = do
        lineEdit <- goateeLineEditNew
        addWidget labelText $ goateeLineEditWidget lineEdit
        return lineEdit

  blackNameEdit <- addLineEdit "Black"
  blackRankEdit <- addLineEdit "Rank"
  blackTeamEdit <- addLineEdit "Team"
  addSeparator
  whiteNameEdit <- addLineEdit "White"
  whiteRankEdit <- addLineEdit "Rank"
  whiteTeamEdit <- addLineEdit "Team"
  addSeparator
  rulesetEdit <- addLineEdit "Ruleset"

  mainTimeWidget <- QWidget.new
  mainTimeBox <- QHBoxLayout.new
  QWidget.setLayout mainTimeWidget mainTimeBox
  mainTimeSpin <- goateeSpinBoxNewWithRange 1 0 3155692600 {- 100 years -} 1
  QDoubleSpinBox.setSuffix (goateeSpinBoxWidget mainTimeSpin) " sec"
  mainTimeLabel <- QLabel.new
  QBoxLayout.addWidget mainTimeBox $ goateeSpinBoxWidget mainTimeSpin
  QBoxLayout.addWidget mainTimeBox mainTimeLabel
  addWidget "Time" mainTimeWidget

  overtimeEdit <- addLineEdit "Overtime"
  gameResultDisplayCheck <- addWideWidget =<< QCheckBox.newWithText "Show game result"
  gameResultEdit <- addLineEdit "Result"
  addSeparator
  gameAnnotatorEdit <- addLineEdit "Annotator"
  gameEntererEdit <- addLineEdit "Enterer"
  addSeparator
  eventNameEdit <- addLineEdit "Event"
  gamePlaceEdit <- addLineEdit "Place"
  gameRoundEdit <- addLineEdit "Round"
  gameDatesEdit <- addLineEdit "Dates"
  gameNameEdit <- addLineEdit "Name"
  gameSourceEdit <- addLineEdit "Source"
  gameCopyrightEdit <- addLineEdit "Copyright"
  addSeparator
  gameOpeningEdit <- addLineEdit "Opening"
  addWideWidget =<< QLabel.newWithText "Game comment:"

  gameCommentTextEdit <- QTextEdit.new
  QTextEdit.setAcceptRichText gameCommentTextEdit False
  -- TODO(Qt) With Qt>=5.2, set placeholder text.
  QBoxLayout.addWidgetWithStretch box gameCommentTextEdit 1

  state <- viewStateNew

  let me = GamePropertiesPanel
        { myUi = ui
        , myState = state
        , myWidget = QWidget.cast scroll

        , myBlackNameEdit = blackNameEdit
        , myBlackRankEdit = blackRankEdit
        , myBlackTeamEdit = blackTeamEdit

        , myWhiteNameEdit = whiteNameEdit
        , myWhiteRankEdit = whiteRankEdit
        , myWhiteTeamEdit = whiteTeamEdit

        , myRulesetEdit = rulesetEdit
        , myMainTimeSpin = mainTimeSpin
        , myMainTimeLabel = mainTimeLabel
        , myOvertimeEdit = overtimeEdit
        , myGameResultDisplayCheck = gameResultDisplayCheck
        , myGameResultEdit = gameResultEdit

        , myGameAnnotatorEdit = gameAnnotatorEdit
        , myGameEntererEdit = gameEntererEdit

        , myEventNameEdit = eventNameEdit
        , myGamePlaceEdit = gamePlaceEdit
        , myGameRoundEdit = gameRoundEdit
        , myGameDatesEdit = gameDatesEdit
        , myGameNameEdit = gameNameEdit
        , myGameSourceEdit = gameSourceEdit
        , myGameCopyrightEdit = gameCopyrightEdit

        , myGameOpeningEdit = gameOpeningEdit
        , myGameCommentTextEdit = gameCommentTextEdit
        }

  initialize me
  return me

initialize :: UiCtrl go ui => GamePropertiesPanel ui -> IO ()
initialize me = do
  let ui = myUi me
  register me [AnyEvent gameInfoChangedEvent]
  viewUpdate me

  connectEdit me (myBlackNameEdit me) gameInfoBlackName $ \x info ->
    info { gameInfoBlackName = x }
  connectEdit me (myBlackRankEdit me) gameInfoBlackRank $ \x info ->
    info { gameInfoBlackRank = x }
  connectEdit me (myBlackTeamEdit me) gameInfoBlackTeamName $ \x info ->
    info { gameInfoBlackTeamName = x }

  connectEdit me (myWhiteNameEdit me) gameInfoWhiteName $ \x info ->
    info { gameInfoWhiteName = x }
  connectEdit me (myWhiteRankEdit me) gameInfoWhiteRank $ \x info ->
    info { gameInfoWhiteRank = x }
  connectEdit me (myWhiteTeamEdit me) gameInfoWhiteTeamName $ \x info ->
    info { gameInfoWhiteTeamName = x }

  connectEdit me (myRulesetEdit me) gameInfoRuleset $ \x info -> info { gameInfoRuleset = x }
  connect_ (goateeSpinBoxWidget $ myMainTimeSpin me) QDoubleSpinBox.valueChangedDoubleSignal $ \_ ->
    setMainTimeLabel me =<< goateeSpinBoxGetValue (myMainTimeSpin me)
  -- Do a first-time initialization on the label as well, since this wouldn't
  -- happen otherwise:
  setMainTimeLabel me =<< goateeSpinBoxGetValue (myMainTimeSpin me)
  doConnect me (goateeSpinBoxOnSpinned $ myMainTimeSpin me) $ \x info ->
    info { gameInfoBasicTimeSeconds = if x == 0 then Nothing else Just x }
  connectEdit me (myOvertimeEdit me) gameInfoOvertime $ \x info -> info { gameInfoOvertime = x }
  let gameResultCheck = myGameResultDisplayCheck me
  connect_ gameResultCheck QAbstractButton.toggledSignal $ \_ -> viewUpdate me
  connectEdit' me (myGameResultEdit me) (QAbstractButton.isChecked gameResultCheck) gameInfoResult $
    \x info -> info { gameInfoResult = x }

  connectEdit me (myGameAnnotatorEdit me) gameInfoAnnotatorName $ \x info ->
    info { gameInfoAnnotatorName = x }
  connectEdit me (myGameEntererEdit me) gameInfoEntererName $ \x info ->
    info { gameInfoEntererName = x }

  connectEdit me (myEventNameEdit me) gameInfoEvent $ \x info -> info { gameInfoEvent = x }
  connectEdit me (myGamePlaceEdit me) gameInfoPlace $ \x info -> info { gameInfoPlace = x }
  connectEdit me (myGameRoundEdit me) gameInfoRound $ \x info -> info { gameInfoRound = x }
  connectEdit me (myGameDatesEdit me) gameInfoDatesPlayed $ \x info ->
    info { gameInfoDatesPlayed = x }
  connectEdit me (myGameNameEdit me) gameInfoGameName $ \x info -> info { gameInfoGameName = x }
  connectEdit me (myGameSourceEdit me) gameInfoSource $ \x info -> info { gameInfoSource = x }
  connectEdit me (myGameCopyrightEdit me) gameInfoCopyright $ \x info ->
    info { gameInfoCopyright = x }

  connectEdit me (myGameOpeningEdit me) gameInfoOpeningComment $ \x info ->
    info { gameInfoOpeningComment = x }

  connect_ (myGameCommentTextEdit me) QTextEdit.textChangedSignal $ do
    value <- QTextEdit.toPlainText $ myGameCommentTextEdit me
    doUiGo ui $ void $ modifyGameInfo $ \info ->
      info { gameInfoGameComment = if null value then Nothing else Just $ stringToSgf value }

-- | @connectEdit me edit getter setter@ binds a 'LineEdit' to a field
-- in the current 'GameInfo' so that changes in one affect the other.
-- Empty strings are coerced to 'Nothing' and vice versa.
--
-- When a 'LineEdit' is changed, we immediately write the value back to
-- the model: we can't wait until focus out because e.g. opening menus
-- doesn't fire focus-out events.  We also inhibit this widget's model
-- change handler assigning back to the edit, because e.g. we don't
-- want to canonicalize "B+1.0" to "B+1" as soon as it's typed.
-- Instead, we do the model-to-view canonicalizing update on
-- focus-out.
connectEdit :: (UiCtrl go ui, Stringlike a)
            => GamePropertiesPanel ui
            -> GoateeLineEdit
            -> (GameInfo -> Maybe a)
            -> (Maybe a -> GameInfo -> GameInfo)
            -> IO ()
connectEdit me edit = connectEdit' me edit (return True)

-- | This is like 'connectEdit'.  The additional @IO Bool@ can return false to
-- indicate that a change in the edit should not be written to the model;
-- returning true writes the change.
connectEdit' :: (UiCtrl go ui, Stringlike a)
              => GamePropertiesPanel ui
              -> GoateeLineEdit
              -> IO Bool
              -> (GameInfo -> Maybe a)
              -> (Maybe a -> GameInfo -> GameInfo)
              -> IO ()
connectEdit' me edit propagateChangesToModel modelGetter modelSetter = do
  let ui = myUi me
  goateeLineEditOnChange edit $ \value -> do
    propagate <- propagateChangesToModel
    when propagate $ doUiGo ui $ void $ modifyGameInfo $ modelSetter $
      if null value then Nothing else Just $ stringToSgf value
  onEvent (goateeLineEditWidget edit) $ \(e :: QFocusEvent.QFocusEvent) -> do
    lostFocus <- QFocusEvent.lostFocus e
    when lostFocus $ do
      cursor <- readCursor ui
      goateeLineEditSetText edit $ maybe "" sgfToString $ modelGetter $
        boardGameInfo $ cursorBoard cursor
    return False
  return ()

-- | @connect me connectFn setter@ binds a widget to a field in the current
-- 'GameInfo' so that changes in one affect the other.  @connect@ constructs an
-- @a -> IO ()@ handler that takes a value and puts it into the model using
-- @setter@, and immediately gives it to @connectFn@, which is in charge of
-- registering the handler.
doConnect :: UiCtrl go ui
        => GamePropertiesPanel ui
        -> ((a -> IO ()) -> IO ())
        -> (a -> GameInfo -> GameInfo)
        -> IO ()
doConnect me connectFn modelSetter =
  let ui = myUi me
  in connectFn $ \newValue -> doUiGo ui $ void $ modifyGameInfo $ modelSetter newValue

destroy :: UiCtrl go ui => GamePropertiesPanel ui -> IO ()
destroy = viewDestroy

update :: UiCtrl go ui => GamePropertiesPanel ui -> IO ()
update me = do
  cursor <- readCursor $ myUi me
  let info = boardGameInfo $ cursorBoard cursor
  forM_ [ (InfoGetter gameInfoBlackName, myBlackNameEdit)
        , (InfoGetter gameInfoBlackRank, myBlackRankEdit)
        , (InfoGetter gameInfoBlackTeamName, myBlackTeamEdit)

        , (InfoGetter gameInfoWhiteName, myWhiteNameEdit)
        , (InfoGetter gameInfoWhiteRank, myWhiteRankEdit)
        , (InfoGetter gameInfoWhiteTeamName, myWhiteTeamEdit)

        , (InfoGetter gameInfoRuleset, myRulesetEdit)
        , (InfoGetter gameInfoOvertime, myOvertimeEdit)

        , (InfoGetter gameInfoAnnotatorName, myGameAnnotatorEdit)
        , (InfoGetter gameInfoEntererName, myGameEntererEdit)

        , (InfoGetter gameInfoEvent, myEventNameEdit)
        , (InfoGetter gameInfoPlace, myGamePlaceEdit)
        , (InfoGetter gameInfoRound, myGameRoundEdit)
        , (InfoGetter gameInfoDatesPlayed, myGameDatesEdit)
        , (InfoGetter gameInfoGameName, myGameNameEdit)
        , (InfoGetter gameInfoSource, myGameSourceEdit)
        , (InfoGetter gameInfoCopyright, myGameCopyrightEdit)

        , (InfoGetter gameInfoOpeningComment, myGameOpeningEdit)
        ] $ \(InfoGetter getter, edit) ->
    goateeLineEditSetText (edit me) $ extractStringlike $ getter info

  -- Update the "main time" spinner.
  let mainTime = fromMaybe 0 $ gameInfoBasicTimeSeconds info
  goateeSpinBoxSetValue (myMainTimeSpin me) mainTime

  -- Update the game result field.
  let gameResultEdit = myGameResultEdit me
  displayGameResult <- QAbstractButton.isChecked $ myGameResultDisplayCheck me
  QWidget.setEnabled (goateeLineEditWidget gameResultEdit) displayGameResult
  goateeLineEditSetText gameResultEdit $
    if displayGameResult
    then extractStringlike $ gameInfoResult info
    else "(hidden)"

  -- Update the game comment TextView.
  let newComment = maybe "" fromText $ gameInfoGameComment info
  oldComment <- QTextEdit.toPlainText $ myGameCommentTextEdit me
  when (newComment /= oldComment) $
    QTextEdit.setPlainText (myGameCommentTextEdit me) newComment

-- | Updates the contents of the label for the main game time, pretty-printing a
-- given number of seconds.
setMainTimeLabel :: UiCtrl go ui => GamePropertiesPanel ui -> BF.Bigfloat -> IO ()
setMainTimeLabel me seconds =
  QLabel.setText (myMainTimeLabel me) $ renderSeconds seconds

extractStringlike :: Stringlike a => Maybe a -> String
extractStringlike = maybe "" sgfToString

renderSeconds :: BF.Bigfloat -> String
renderSeconds totalSecondsFloat =
  let isNegative = totalSecondsFloat < 0
      (wholeSeconds, fractionalSecondsStr) = first abs $ splitFloat totalSecondsFloat
      (totalMinutes, seconds) = wholeSeconds `divMod` 60
      (hours, minutes) = totalMinutes `divMod` 60
  in (if isNegative then ('-':) else id) $
     (if hours > 0
      then show hours ++ ':' : show2 minutes ++ ':' : show2 seconds
      else show minutes ++ ':' : show2 seconds) ++
     fractionalSecondsStr
  where show2 n = if n < 10 then '0' : show n else show n

-- | Returns a pair containing the signed whole part of a 'BF.Bigfloat', plus a
-- string containing a decimal place and everything after it, if the float has a
-- fractional part, otherwise an empty string.
splitFloat :: BF.Bigfloat -> (Integer, String)
splitFloat x =
  let xs = show x
      (addNeg, xs') = case xs of
        '-':xs' -> (('-':), xs')
        _ -> (id, xs)
      (hd, tl) = break (== '.') xs'
  in (read $ addNeg hd, tl)
