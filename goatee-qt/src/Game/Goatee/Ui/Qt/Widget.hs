-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

-- | Provides wrappers around regular Qt widgets, adding support for assigning
-- to widget values without firing signal handlers.
module Game.Goatee.Ui.Qt.Widget (
  -- * LineEdit
  GoateeLineEdit, goateeLineEditWidget, goateeLineEditNew,
  goateeLineEditGetText, goateeLineEditSetText,
  goateeLineEditOnChange,
  -- * Spin button
  GoateeSpinBox, goateeSpinBoxNewWithRange, goateeSpinBoxWidget, goateeSpinBoxGetValue,
  goateeSpinBoxSetValue, goateeSpinBoxOnSpinned,
  ) where

import qualified Game.Goatee.Common.Bigfloat as BF
import Game.Goatee.Ui.Qt.Latch
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Widgets.QAbstractSpinBox as QAbstractSpinBox
import qualified Graphics.UI.Qtah.Widgets.QDoubleSpinBox as QDoubleSpinBox
import qualified Graphics.UI.Qtah.Widgets.QLineEdit as QLineEdit
import Game.Goatee.Ui.Qt.Utils

data GoateeLineEdit = GoateeLineEdit QLineEdit.QLineEdit Latch

goateeLineEditNew :: IO GoateeLineEdit
goateeLineEditNew = GoateeLineEdit <$> QLineEdit.new <*> newLatch

goateeLineEditWidget :: GoateeLineEdit -> QLineEdit.QLineEdit
goateeLineEditWidget (GoateeLineEdit lineEdit _) = lineEdit

goateeLineEditGetText :: GoateeLineEdit -> IO String
goateeLineEditGetText (GoateeLineEdit lineEdit _) = QLineEdit.text lineEdit

-- | Sets an line edit's value without firing handlers registered through
-- 'goateeLineEditOnChange'.
goateeLineEditSetText :: GoateeLineEdit -> String -> IO ()
goateeLineEditSetText (GoateeLineEdit lineEdit latch) value =
  withLatchOn latch $ QLineEdit.setText lineEdit value

goateeLineEditOnChange :: GoateeLineEdit -> (String -> IO ()) -> IO ()
goateeLineEditOnChange (GoateeLineEdit lineEdit latch) handler =
  connect_ lineEdit QLineEdit.textChangedSignal $ \value ->
  whenLatchOff latch $ handler value

data GoateeSpinBox = GoateeSpinBox QDoubleSpinBox.QDoubleSpinBox Latch

goateeSpinBoxNewWithRange :: Int -> Double -> Double -> Double -> IO GoateeSpinBox
goateeSpinBoxNewWithRange decimals min max step = do
  spinBox <- QDoubleSpinBox.new
  QDoubleSpinBox.setDecimals spinBox decimals
  QDoubleSpinBox.setRange spinBox min max
  QDoubleSpinBox.setSingleStep spinBox step
  latch <- newLatch
  return $ GoateeSpinBox spinBox latch

goateeSpinBoxWidget :: GoateeSpinBox -> QDoubleSpinBox.QDoubleSpinBox
goateeSpinBoxWidget (GoateeSpinBox spinBox _) = spinBox

goateeSpinBoxGetValue :: GoateeSpinBox -> IO BF.Bigfloat
goateeSpinBoxGetValue (GoateeSpinBox spinBox _) =
  spinBoxGetValueAsBigfloat spinBox

-- | Sets a spin button's value without firing handlers registered through
-- 'goateeSpinBoxOnSpinned'.
goateeSpinBoxSetValue :: GoateeSpinBox -> BF.Bigfloat -> IO ()
goateeSpinBoxSetValue (GoateeSpinBox spinBox latch) value =
  withLatchOn latch $ QDoubleSpinBox.setValue spinBox $ BF.toDouble value

goateeSpinBoxOnSpinned :: GoateeSpinBox -> (BF.Bigfloat -> IO ()) -> IO ()
goateeSpinBoxOnSpinned (GoateeSpinBox spinBox latch) handler =
  connect_ spinBox QAbstractSpinBox.editingFinishedSignal $
  whenLatchOff latch $ spinBoxGetValueAsBigfloat spinBox >>= handler
