-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

-- | A widget that renders an interactive Go board.
module Game.Goatee.Ui.Qt.Goban (
  Goban,
  create,
  destroy,
  myWidget,
  ) where

import Control.Monad ((<=<), forM_, liftM, unless, void, when)
import Data.Bits (zeroBits)
import qualified Data.Foldable as F
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Maybe (fromJust, isJust)
import Data.Tree (drawTree, unfoldTree)
import Foreign.Hoppy.Runtime (fromContents, toCppEnum, withScopedPtr)
import Game.Goatee.Common
import Game.Goatee.Lib.Board hiding (isValidMove)
import Game.Goatee.Lib.Monad (
  AnyEvent (..),
  childAddedEvent,
  childDeletedEvent,
  goDown,
  goLeft,
  goRight,
  goToRoot,
  goUp,
  navigationEvent,
  propertiesModifiedEvent,
  )
import Game.Goatee.Lib.Property
import Game.Goatee.Lib.Tree
import Game.Goatee.Lib.Types
import Game.Goatee.Ui.Qt.Common
import Graphics.UI.Qtah.Core.HRect (HRect (HRect))
import Graphics.UI.Qtah.Core.HPointF (HPointF (HPointF))
import qualified Graphics.UI.Qtah.Core.QEvent as QEvent
import Graphics.UI.Qtah.Core.QEvent (QEvent)
import Graphics.UI.Qtah.Core.QVector.QPointF (QVectorQPointF)
import qualified Graphics.UI.Qtah.Core.Types as Qt
import Graphics.UI.Qtah.Core.Types (QtMouseButton)
import Graphics.UI.Qtah.Core.Types (QReal)
import Graphics.UI.Qtah.Event (onEvent)
import qualified Graphics.UI.Qtah.Gui.HColor as HColor
import Graphics.UI.Qtah.Gui.HColor (HColor)
import qualified Graphics.UI.Qtah.Gui.QBrush as QBrush
import Graphics.UI.Qtah.Gui.QBrush (QBrush)
import qualified Graphics.UI.Qtah.Gui.QKeyEvent as QKeyEvent
import Graphics.UI.Qtah.Gui.QKeyEvent (QKeyEvent)
import qualified Graphics.UI.Qtah.Gui.QMouseEvent as QMouseEvent
import Graphics.UI.Qtah.Gui.QMouseEvent (QMouseEvent)
import qualified Graphics.UI.Qtah.Gui.QPainter as QPainter
import Graphics.UI.Qtah.Gui.QPainter (QPainter)
import qualified Graphics.UI.Qtah.Gui.QPainterPath as QPainterPath
import Graphics.UI.Qtah.Gui.QPainterPath (QPainterPath)
import Graphics.UI.Qtah.Gui.QPaintEvent (QPaintEvent)
import qualified Graphics.UI.Qtah.Gui.QPen as QPen
import Graphics.UI.Qtah.Gui.QPen (QPen)
import qualified Graphics.UI.Qtah.Gui.QPolygonF as QPolygonF
import Graphics.UI.Qtah.Gui.QPolygonF (QPolygonF)
import qualified Graphics.UI.Qtah.Gui.QTransform as QTransform
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget
import Graphics.UI.Qtah.Widgets.QWidget (QWidget)

-- | If false, then the up and down keys will move toward and away
-- from the game tree root, and left and right will move between
-- siblings.  If true, these are reversed.
useHorizontalKeyNavigation :: Bool
useHorizontalKeyNavigation = True

-- Key handler code below requires that these keys don't use modifiers.
keyNavActions :: UiCtrl go ui => Map Qt.QtKey (ui -> IO ())
keyNavActions =
  Map.fromList $
  --map (fmap ((>> return ()) .))  -- Drop the booleans these actions return.
  map (fmap $ \action ui -> doUiGo ui $ void action)
      (if useHorizontalKeyNavigation
       then [ (Qt.KeyUp, goLeft)
            , (Qt.KeyDown, goRight)
            , (Qt.KeyLeft, goUp)
            , (Qt.KeyRight, goDown 0)
            ]
       else [ (Qt.KeyUp, goUp)
            , (Qt.KeyDown, goDown 0)
            , (Qt.KeyLeft, goLeft)
            , (Qt.KeyRight, goRight)
            ]) ++
  [ (Qt.KeyHome, flip doUiGo goToRoot)
  , (Qt.KeyEnd, flip doUiGo $ whileM (goDown 0) $ return ())
  , (Qt.KeyPageUp, flip doUiGo $ void $ andM $ replicate 10 goUp)
  , (Qt.KeyPageDown, flip doUiGo $ void $ andM $ replicate 10 $ goDown 0)
  ]

boardBgColor :: Rgb
boardBgColor = rgb 229 178 58

gridColor :: Rgb
gridColor = rgb 0 0 0

blackStoneColor :: Rgb
blackStoneColor = rgb 0 0 0

blackStoneBorderColor :: Rgb
blackStoneBorderColor = rgb 255 255 255

whiteStoneColor :: Rgb
whiteStoneColor = rgb 255 255 255

whiteStoneBorderColor :: Rgb
whiteStoneBorderColor = rgb 0 0 0

stoneColor :: Color -> Rgb
stoneColor color = case color of
  Black -> blackStoneColor
  White -> whiteStoneColor

stoneBorderColor :: Color -> Rgb
stoneBorderColor color = case color of
  Black -> blackStoneBorderColor
  White -> whiteStoneBorderColor

-- | Percentage of coordinate size, in @[0, 1]@.
stoneBorderThickness :: QReal
stoneBorderThickness = 0.03

-- | The fill colour of the dot shown over the last played stone.
currentStoneMarkerColor :: Rgb
currentStoneMarkerColor = rgb 0 0 255

-- | The radius of the dot shown over the last played stone.  Percentage of
-- coordinate size, in @[0, 1]@.
currentStoneMarkerRadius :: QReal
currentStoneMarkerRadius = 0.15

-- | The border thickness of the dot shown over the last played stone.
-- Percentage of coordinate size, in @[0, 1]@.
currentStoneMarkerBorderThickness :: QReal
currentStoneMarkerBorderThickness = 0.02

-- | The radius of small circles that are overlaid on points to indicate that
-- move variations exist.  Percentage of coordinate size, in @[0, 1]@.
stoneVariationRadius :: QReal
stoneVariationRadius = 0.15

-- | The width of the border of a variation circle.  Percentage of coordinate
-- size, in @[0, 1]@.
stoneVariationBorderThickness :: QReal
stoneVariationBorderThickness = 0.02

-- | The radius of star points.  Percentage of coordinate size, in @[0, 1]@.
starPointRadius :: QReal
starPointRadius = 0.1

-- | The opacity, in @[0, 1]@, of a stone that should be drawn dimmed because of
-- 'DD'.
dimmedPointOpacity :: QReal
dimmedPointOpacity = 0.3

-- | Returns the color that should be used to draw a 'Mark' on either an empty
-- point, or one with a stone of the given color.
coordAnnotationStrokeColor :: Maybe Color -> Rgb
coordAnnotationStrokeColor = maybe blackStoneColor stoneBorderColor

-- | For line and arrow annotations, the colour of the line.
boardAnnotationLineColor :: Rgb
boardAnnotationLineColor = rgb 0 0 0

-- | For line and arrow annotations, the width of the line.  1 is the width of a
-- stone.
boardAnnotationLineWidth :: QReal
boardAnnotationLineWidth = 0.08

-- | For arrow annotations, the distance to pull back along the length of a line
-- before extending at right angles to form the arrowhead.
boardAnnotationArrowPullback :: QReal
boardAnnotationArrowPullback = 0.2

-- | For arrow annotations, the distance to extend away from the baseline in
-- either direction to form the arrowhead.
boardAnnotationArrowWidth :: QReal
boardAnnotationArrowWidth = 0.1

-- | A GTK widget that renders a Go board.
--
-- @ui@ should be an instance of 'UiCtrl'.
data Goban ui = Goban
  { myUi :: ui
  , myState :: ViewState
  , myWidget :: QWidget
  , myModesChangedHandler :: IORef (Maybe Registration)
  }

instance UiCtrl go ui => UiView go ui (Goban ui) where
  viewName = const "Goban"
  viewCtrl = myUi
  viewState = myState
  viewUpdate = update

-- | Creates a 'Goban' for rendering Go boards of the given size.
create :: UiCtrl go ui => ui -> IO (Goban ui)
create ui = do
  widget <- QWidget.new
  QWidget.setMouseTracking widget True

  state <- viewStateNew
  modesChangedHandler <- newIORef Nothing

  let me = Goban { myUi = ui
                 , myState = state
                 , myWidget = widget
                 , myModesChangedHandler = modesChangedHandler
                 }

  -- Event handlers are fired in order opposite to registration order, so by
  -- registering this super-generic handler first, the more specific handlers
  -- below will actually get a chance to handle events before falling back to
  -- this one.
  onEvent widget $ \(event :: QEvent) ->
    QEvent.eventType event >>= \case
      QEvent.Leave -> handleMouseMove me Nothing >> return True
      _ -> return False

  onEvent widget $ \(_ :: QPaintEvent) -> do
    drawBoard me
    return True

  onEvent widget $ \(event :: QMouseEvent) ->
    QEvent.eventType event >>= \case
      QEvent.MouseButtonPress -> do
        button <- QMouseEvent.button event
        x <- QMouseEvent.x event
        y <- QMouseEvent.y event
        handleMouseDown me button (x, y)
        return True

      QEvent.MouseButtonRelease -> do
        button <- QMouseEvent.button event
        x <- QMouseEvent.x event
        y <- QMouseEvent.y event
        handleMouseUp me button (x, y)
        return True

      QEvent.MouseMove -> do
        x <- QMouseEvent.x event
        y <- QMouseEvent.y event
        handleMouseMove me $ Just (x, y)
        return True

      _ -> return False

  onEvent widget $ \(e :: QKeyEvent) -> do
    key :: Qt.QtKey <- toCppEnum . fromIntegral <$> QKeyEvent.key e
    eventType <- QEvent.eventType e
    case eventType of
      QEvent.KeyPress -> do
        mods <- QKeyEvent.modifiers e
        let km = (key, mods)
        let maybeAction = Map.lookup key keyNavActions
        cond (return False)
          [(isJust maybeAction && mods == zeroBits,
            fromJust maybeAction ui >> return True),

            -- Write a list of the current node's properties to the console.
           (km == (Qt.KeyT, zeroBits), do
               cursor <- readCursor ui
               print $ nodeProperties $ cursorNode cursor
               return True),

            -- Draw a tree rooted at the current node to the console.
           (km == (Qt.KeyT, Qt.shiftModifier), do
               cursor <- readCursor ui
               putStrLn $ drawTree $ flip unfoldTree (cursorNode cursor) $ \node ->
                 (show $ nodeProperties node, nodeChildren node)
               return True)]

      _ -> return False  -- Other event types.

  initialize me
  return me

initialize :: UiCtrl go ui => Goban ui -> IO ()
initialize me = do
  let ui = myUi me
  register me
    [ AnyEvent childAddedEvent
    , AnyEvent childDeletedEvent
    , AnyEvent navigationEvent
    , AnyEvent propertiesModifiedEvent
    ]
  writeIORef (myModesChangedHandler me) =<<
    liftM Just (registerModesChangedHandler ui "Goban" $ \_ _ -> update me)
  update me

destroy :: UiCtrl go ui => Goban ui -> IO ()
destroy me = do
  let ui = myUi me
  F.mapM_ (unregisterModesChangedHandler ui) =<< readIORef (myModesChangedHandler me)
  viewDestroy me

update :: UiCtrl go ui => Goban ui -> IO ()
update me = do
  fireGobanEvent me GobanInvalidate
  redraw me

-- | Notifies the active tool that a mouse button was pressed down over the
-- board.
handleMouseDown :: UiCtrl go ui => Goban ui -> QtMouseButton -> (Int, Int) -> IO ()
handleMouseDown me mouseButton mouseCoord = do
  QWidget.setFocus $ myWidget me  -- TODO Pass Qt.MouseFocusReason here.
  maybeCoord <- screenToBoardCoordinates me mouseCoord
  fireGobanEvent me $ GobanClickStart mouseButton maybeCoord

-- | Notifies the active tool that a mouse click or drag that started with the
-- mouse being pressed down over the board has completed.
handleMouseUp :: UiCtrl go ui => Goban ui -> QtMouseButton -> (Int, Int) -> IO ()
handleMouseUp me mouseButton mouseCoord = do
  maybeCoord <- screenToBoardCoordinates me mouseCoord
  fireGobanEvent me $ GobanClickFinish mouseButton maybeCoord

-- | notifies the active tool that the mouse has moved over the board.
handleMouseMove :: UiCtrl go ui => Goban ui -> Maybe (Int, Int) -> IO ()
handleMouseMove me maybeMouseCoord = do
  maybeCoord <- maybe (return Nothing) (screenToBoardCoordinates me) maybeMouseCoord
  fireGobanEvent me $ GobanMouseMove maybeCoord

-- | Sends an event to the active tool.
fireGobanEvent :: UiCtrl go ui => Goban ui -> GobanEvent -> IO ()
fireGobanEvent me event = do
  AnyTool tool <- readTool $ myUi me
  doRedraw <- toolGobanHandleEvent tool event
  when doRedraw $ redraw me

applyBoardCoordinates :: QPainter -> BoardState -> Int -> Int -> IO ()
applyBoardCoordinates painter board widgetWidth widgetHeight = do
  let boardWidth' = fromIntegral (boardWidth board)
      boardHeight' = fromIntegral (boardHeight board)
      -- We've got to subtract 1 here so that we don't run 1 pixel over the edge
      -- of the canvas along the bounding axis.
      --
      -- TODO Do we really need this? ^
      canvasWidth = fromIntegral widgetWidth - 1 :: QReal
      canvasHeight = fromIntegral widgetHeight - 1
      maxStoneWidth = canvasWidth / boardWidth'
      maxStoneHeight = canvasHeight / boardHeight'
      maxStoneLength = min maxStoneWidth maxStoneHeight
      marginX = max 0 $ (canvasWidth - boardWidth' * maxStoneLength) / 2
      marginY = max 0 $ (canvasHeight - boardHeight' * maxStoneLength) / 2

  -- Set user coordinates so that the top-left stone occupies the rectangle
  -- from (0,0) to (1,1).
  QPainter.translateRaw painter marginX marginY
  QPainter.scaleRaw painter maxStoneLength maxStoneLength

-- | Takes a screen coordinate and returns the corresponding board coordinate,
-- or @Nothing@ if the screen coordinate is not over a square on the board.
screenToBoardCoordinates :: UiCtrl go ui => Goban ui -> (Int, Int) -> IO (Maybe (Int, Int))
screenToBoardCoordinates me (x, y) = do
  let ui = myUi me
      widget = myWidget me
  board <- cursorBoard <$> readCursor ui
  widgetWidth <- QWidget.width widget
  widgetHeight <- QWidget.height widget

  -- TODO Don't just copy this from 'applyBoardCoordinates'.
  let boardWidth' = fromIntegral (boardWidth board)
      boardHeight' = fromIntegral (boardHeight board)
      -- We've got to subtract 1 here so that we don't run 1 pixel over the edge
      -- of the canvas along the bounding axis.
      --
      -- TODO Do we really need this? ^
      canvasWidth = fromIntegral widgetWidth - 1 :: QReal
      canvasHeight = fromIntegral widgetHeight - 1
      maxStoneWidth = canvasWidth / boardWidth'
      maxStoneHeight = canvasHeight / boardHeight'
      maxStoneLength = min maxStoneWidth maxStoneHeight
      marginX = max 0 $ (canvasWidth - boardWidth' * maxStoneLength) / 2
      marginY = max 0 $ (canvasHeight - boardHeight' * maxStoneLength) / 2

  transform <- withScopedPtr (QTransform.new2x2 1 0 0 1 0 0) $ \txf -> do
    QTransform.translate txf marginX marginY
    QTransform.scale txf maxStoneLength maxStoneLength
    QTransform.inverted txf

  HPointF bx by <-
    QTransform.mapPointF transform $ HPointF (fromIntegral x) (fromIntegral y)
  let bx' = floor bx
      by' = floor by
  return $ if bx' < 0 || bx' >= boardWidth board ||
              by' < 0 || by' >= boardHeight board
           then Nothing
           else Just (bx', by')

-- | Schedules the goban to repaint.
redraw :: UiCtrl go ui => Goban ui -> IO ()
redraw = QWidget.update . myWidget

-- | Fully redraws the board based on the current controller and UI state.
drawBoard :: UiCtrl go ui => Goban ui -> IO ()
drawBoard me = do
  let ui = myUi me
      widget = myWidget me

  cursor <- readCursor ui
  modes <- readModes ui
  AnyTool tool <- readTool ui

  board <- toolGobanRenderGetBoard tool cursor

  let variationMode = rootInfoVariationMode $ gameInfoRootInfo $ boardGameInfo $ cursorBoard cursor

      variations :: [(Coord, Color)]
      variations = if variationModeBoardMarkup variationMode
                   then cursorVariations (variationModeSource variationMode) cursor
                   else []

      -- Positions of stones that have been played at the current node.
      current :: [Coord]
      current = if uiHighlightCurrentMovesMode modes
                then concatMap (\prop -> case prop of
                                   B (Just xy) -> [xy]
                                   W (Just xy) -> [xy]
                                   _ -> []) $
                     cursorProperties cursor
                else []

      -- | Performs processing at the individual coord level based on UI state.
      preprocessCoord :: CoordState -> CoordState
      preprocessCoord =
        let applyStoneViewMode = case uiViewStonesMode modes of
              ViewStonesRegularMode -> id
              ViewStonesOneColorMode -> coerceStone $ uiViewStonesOneColorModeColor modes
              ViewStonesBlindMode -> setStone Nothing
        in applyStoneViewMode

      -- | Replaces an existing stone of color opposite to the one given with a
      -- stone of the given color.
      coerceStone :: Color -> CoordState -> CoordState
      coerceStone color state = if coordStone state == Just (cnot color)
                                then state { coordStone = Just color }
                                else state

      -- | Replaces a coordinate's stone.
      setStone :: Maybe Color -> CoordState -> CoordState
      setStone color state = if coordStone state == color
                             then state
                             else state { coordStone = color }

  -- The state of the board's points, with all data for rendering.
  renderedCoords :: [[RenderedCoord]] <-
    toolGobanRenderModifyCoords tool board $
    -- Add current moves.
    (flip .)
    foldr (\(x, y) grid ->
            listUpdate (flip listUpdate x $
                        \renderedCoord -> renderedCoord { renderedCoordCurrent = True })
                        y
                        grid)
          current $
    -- Add variations.
    foldr (\((x, y), color) grid ->
            listUpdate (flip listUpdate x $
                        \renderedCoord -> renderedCoord { renderedCoordVariation = Just color })
                        y
                        grid)
          (map (map $ (\state -> RenderedCoord state False Nothing) . preprocessCoord) $
           boardCoordStates board)
          variations

  withScopedPtr (QPainter.newWithDevice widget) $ \painter ->
    withDrawingImplements $ \implements -> do

    -- Start with antialiasing on.  Individual draw* methods will override this
    -- and not restore it, so it is up to each draw* method to set antialiasing
    -- on or off as it wants.
    QPainter.setRenderHint painter QPainter.Antialiasing

    -- Fill the background a nice woody shade.
    widgetWidth <- QWidget.width widget
    widgetHeight <- QWidget.height widget
    QPainter.fillRectWithColor painter (HRect 0 0 widgetWidth widgetHeight) boardBgColor

    -- Switch to a coordinate system convenient for drawing the board.
    applyBoardCoordinates painter board widgetWidth widgetHeight

    forIndexM_ renderedCoords $ \y row ->
      forIndexM_ row $ \x renderedCoord -> do
        let coord = renderedCoordState renderedCoord
        -- TODO Handle dimmed coordinates.
        when (coordVisible coord) $
          drawCoord painter implements board x y renderedCoord

    -- Draw non-CoordState-based annotations.
    -- Lines:
    unless (null (boardLines board)) $ do
      QPainter.setRenderHint painter QPainter.Antialiasing
      QPainter.setPen painter $ annotationLinePen implements
      forM_ (boardLines board) $ \(Line from to) -> drawLine painter from to

    -- Arrows:
    unless (null (boardArrows board)) $ do
      QPainter.setRenderHint painter QPainter.Antialiasing
      withScopedPtr (QPen.newWithColor boardAnnotationLineColor) $ \arrowheadPen ->
        withScopedPtr (QBrush.newWithColor boardAnnotationLineColor) $ \arrowheadBrush ->
        withScopedPtr QPainterPath.new $ \arrowheadPath -> do

        QPen.setCapStyle arrowheadPen Qt.FlatCap
        QPen.setWidth arrowheadPen 0

        QPainterPath.moveToPointF arrowheadPath (HPointF 0 (-boardAnnotationArrowWidth))
        QPainterPath.lineToPointF arrowheadPath (HPointF 0 boardAnnotationArrowWidth)
        QPainterPath.lineToPointF arrowheadPath (HPointF boardAnnotationArrowPullback 0)
        QPainterPath.closeSubpath arrowheadPath

        forM_ (boardArrows board) $ \(from, to) ->
          drawArrow painter
                    (annotationLinePen implements)
                    arrowheadPen
                    arrowheadBrush
                    arrowheadPath
                    from
                    to

  return ()

data DrawingImplements = DrawingImplements
  { gridPen :: QPen
  , gridBorderPen :: QPen
  , starPen :: QPen
  , starBrush :: QBrush
  , stonePen :: Color -> QPen
  , stoneBrush :: Color -> QBrush
  , currentStoneMarkerPen :: QPen
  , currentStoneMarkerBrush :: QBrush
  , variationMarkerPen :: Color -> QPen
  , variationMarkerBrush :: Color -> QBrush
  , markPen :: Maybe Color -> QPen
  , markTrianglePolygon :: QPolygonF
  , markSquarePolygon :: QPolygonF
  , markSelectedPolygon :: QPolygonF
  , annotationLinePen :: QPen
  , noBrush :: QBrush
  }

withDrawingImplements :: (DrawingImplements -> IO a) -> IO a
withDrawingImplements action =
  withScopedPtr (QPen.newWithColor gridColor) $ \gridPen ->
  withScopedPtr (QPen.newWithColor gridColor) $ \gridBorderPen ->
  withScopedPtr (QPen.newWithColor gridColor) $ \starPen ->
  withScopedPtr (QBrush.newWithColor gridColor) $ \starBrush ->
  withScopedPtr (QPen.newWithColor $ stoneBorderColor Black) $ \blackStonePen ->
  withScopedPtr (QPen.newWithColor $ stoneBorderColor White) $ \whiteStonePen ->
  withScopedPtr (QBrush.newWithColor $ stoneColor Black) $ \blackStoneBrush ->
  withScopedPtr (QBrush.newWithColor $ stoneColor White) $ \whiteStoneBrush ->
  withScopedPtr (QPen.newWithColor $ stoneColor Black) $ \currentStoneMarkerPen ->
  withScopedPtr (QBrush.newWithColor $ currentStoneMarkerColor) $ \currentStoneMarkerBrush ->
  withScopedPtr (QPen.newWithColor $ stoneBorderColor Black) $ \blackVariationMarkerPen ->
  withScopedPtr (QPen.newWithColor $ stoneBorderColor White) $ \whiteVariationMarkerPen ->
  withScopedPtr (QBrush.newWithColor $ stoneColor Black) $ \blackVariationMarkerBrush ->
  withScopedPtr (QBrush.newWithColor $ stoneColor White) $ \whiteVariationMarkerBrush ->
  withScopedPtr (QPen.newWithColor blackStoneColor) $ \blackMarkPen ->
  withScopedPtr (QPen.newWithColor whiteStoneColor) $ \whiteMarkPen ->
  withScopedPtr (withScopedPtr (fromContents markTrianglePoints :: IO QVectorQPointF)
                 QPolygonF.newWithPoints) $ \markTrianglePolygon ->
  withScopedPtr (withScopedPtr (fromContents markSquarePoints :: IO QVectorQPointF)
                 QPolygonF.newWithPoints) $ \markSquarePolygon ->
  withScopedPtr (withScopedPtr (fromContents markSelectedPoints :: IO QVectorQPointF)
                 QPolygonF.newWithPoints) $ \markSelectedPolygon ->
  withScopedPtr (QPen.newWithColor boardAnnotationLineColor) $ \annotationLinePen ->
  withScopedPtr QBrush.new $ \noBrush -> do
    QPen.setCosmetic gridPen True
    QPen.setCosmetic gridBorderPen True
    QPen.setWidth gridPen 1
    QPen.setWidth gridBorderPen 2
    QPen.setWidth starPen 0
    forM_ [blackStonePen, whiteStonePen] $ \pen ->
      QPen.setWidthF pen stoneBorderThickness
    forM_ [blackMarkPen, whiteMarkPen] $ \pen -> do
      QPen.setWidthF pen 0.1
      QPen.setJoinStyle pen Qt.MiterJoin
    QPen.setWidthF currentStoneMarkerPen currentStoneMarkerBorderThickness
    QPen.setWidthF blackVariationMarkerPen stoneVariationBorderThickness
    QPen.setWidthF whiteVariationMarkerPen stoneVariationBorderThickness
    QPen.setCapStyle annotationLinePen Qt.FlatCap
    QPen.setWidthF annotationLinePen boardAnnotationLineWidth

    let implements = DrawingImplements
          { gridPen = gridPen
          , gridBorderPen = gridBorderPen
          , starPen = starPen
          , starBrush = starBrush
          , stonePen = \color -> case color of
              Black -> blackStonePen
              White -> whiteStonePen
          , stoneBrush = \color -> case color of
              Black -> blackStoneBrush
              White -> whiteStoneBrush
          , currentStoneMarkerPen = currentStoneMarkerPen
          , currentStoneMarkerBrush = currentStoneMarkerBrush
          , variationMarkerPen = \color -> case color of
              Black -> blackVariationMarkerPen
              White -> whiteVariationMarkerPen
          , variationMarkerBrush = \color -> case color of
              Black -> blackVariationMarkerBrush
              White -> whiteVariationMarkerBrush
          , markPen = \color -> case color of
              Nothing -> blackMarkPen
              Just White -> blackMarkPen
              Just Black -> whiteMarkPen
          , markTrianglePolygon = markTrianglePolygon
          , markSquarePolygon = markSquarePolygon
          , markSelectedPolygon = markSelectedPolygon
          , annotationLinePen = annotationLinePen
          , noBrush = noBrush
          }

    action implements

-- | Draws a single point on the board.
drawCoord :: QPainter
          -> DrawingImplements
          -> BoardState
             -- ^ The board being drawn.
          -> Int
             -- ^ The x-index of the point to be drawn.
          -> Int
             -- ^ The y-index of the point to be drawn.
          -> RenderedCoord
             -- ^ The point to be drawn.
          -> IO ()
drawCoord painter implements board x y renderedCoord = do
  let x' = fromIntegral x
      y' = fromIntegral y
      coord = renderedCoordState renderedCoord
      current = renderedCoordCurrent renderedCoord
      variation = renderedCoordVariation renderedCoord
  -- Translate the grid so that we can draw the stone from (0,0) to (1,1).
  QPainter.translateRaw painter x' y'
  -- Draw the grid, stone or star (if present), and mark (if present).
  maybe (do drawGrid painter implements board x y
            when (coordStar coord) $ drawStar painter implements)
        (\color -> drawStone painter implements color) $
    coordStone coord
  maybe (return ()) (drawMark painter implements $ coordStone coord) $ coordMark coord
  case (current, variation) of
    -- This is the case of @VariationMode ShowChildVariations True@ with a
    -- suicide play followed by an immediate fill.  Of course, this shouldn't
    -- normally happen.
    (True, Just variation') -> do drawCurrent painter implements True
                                  drawVariation painter implements variation' True
    (True, _) -> drawCurrent painter implements False
    (_, Just variation') -> drawVariation painter implements variation' False
    _ -> return ()
  -- Restore the coordinate system for the next stone.
  QPainter.translateRaw painter (-x') (-y')

-- | Draws the gridlines for a single point on the board.
drawGrid :: QPainter -> DrawingImplements -> BoardState -> Int -> Int -> IO ()
drawGrid painter implements board x y = do
  -- Draw the grid.
  let atLeft = x == 0
      atTop = y == 0
      atRight = x == boardWidth board - 1
      atBottom = y == boardHeight board - 1
      gridX0 = if atLeft then 0.5 else 0
      gridY0 = if atTop then 0.5 else 0
      gridX1 = if atRight then 0.5 else 1
      gridY1 = if atBottom then 0.5 else 1

  -- Disable antialiasing for sharp grid lines.
  QPainter.setRenderHintTo painter QPainter.Antialiasing False

  -- Draw the horizontal line.
  QPainter.setPen painter $
    (if atTop || atBottom then gridBorderPen else gridPen) implements
  QPainter.drawLinePointF painter (HPointF gridX0 0.5) (HPointF gridX1 0.5)

  -- Draw the vertical line.
  QPainter.setPen painter $
    (if atLeft || atRight then gridBorderPen else gridPen) implements
  QPainter.drawLinePointF painter (HPointF 0.5 gridY0) (HPointF 0.5 gridY1)

-- | Draws a stone from @(0, 0)@ to @(1, 1)@ in user coordinates.
drawStone :: QPainter -> DrawingImplements -> Color -> IO ()
drawStone painter implements color = do
  -- Use antialiasing for smooth stones.
  QPainter.setRenderHint painter QPainter.Antialiasing

  QPainter.setPen painter $ stonePen implements color
  QPainter.setBrush painter $ stoneBrush implements color

  QPainter.drawEllipsePointF painter
    (HPointF 0.5 0.5)
    (0.5 - stoneBorderThickness / 2) (0.5 - stoneBorderThickness / 2)

-- | Draws a dot to indicate that the current point is a star point.
drawStar :: QPainter -> DrawingImplements -> IO ()
drawStar painter implements = do
  QPainter.setRenderHint painter QPainter.Antialiasing

  QPainter.setPen painter $ starPen implements
  QPainter.setBrush painter $ starBrush implements

  QPainter.drawEllipsePointF painter (HPointF 0.5 0.5) starPointRadius starPointRadius

  -- TODO Switch to aliased rendering if the canvas is small enough; see the old
  -- goatee-gtk logic below.
{-
  setSourceRGB 0 0 0
  -- This seems to be a decent point to transition from an antialiased star to
  -- an aliased star (well, box), balancing transitioning too early (having a
  -- jump in size) with too late (and having ugly antialiased bouncing star
  -- points for a range).
  let minRadiusOnScreen = 1.8
  (radiusOnScreen, _) <- userToDeviceDistance starPointRadius 0
  (cx, cy) <- roundToPixels 0.5 0.5
  if radiusOnScreen >= minRadiusOnScreen
    then do arc cx cy starPointRadius 0 pi_2
            fill
    else do setAntialias AntialiasNone
            (pixel, _) <- deviceToUserDistance 1 0
            rectangle (cx - 2 * pixel) (cy - 2 * pixel) (3 * pixel) (3 * pixel)
            fill
            setAntialias AntialiasDefault
-}

-- | Draws the given mark on the current point.  The color should be that of the
-- stone on the point, if there is one; it determines the color of the mark.
drawMark :: QPainter -> DrawingImplements -> Maybe Color -> Mark -> IO ()
drawMark painter implements stone mark = do
  QPainter.setRenderHint painter QPainter.Antialiasing
  QPainter.setPen painter $ markPen implements stone
  QPainter.setBrush painter $ noBrush implements

  case mark of
    MarkCircle ->
      QPainter.drawEllipsePointF painter (HPointF 0.5 0.5) 0.25 0.25
    MarkTriangle -> QPainter.drawConvexPolygonF painter $ markTrianglePolygon implements
    MarkSquare -> QPainter.drawConvexPolygonF painter $ markSquarePolygon implements
    MarkSelected -> QPainter.drawConvexPolygonF painter $ markSelectedPolygon implements
    MarkX -> do
      QPainter.drawLinePointF painter (HPointF 0.25 0.25) (HPointF 0.75 0.75)
      QPainter.drawLinePointF painter (HPointF 0.25 0.75) (HPointF 0.75 0.25)

-- The coordinates for inscribing a triangle within a unit circle centered about
-- @(0.5, 0.5)@, with radius @triangleRadius@.
triangleRadius, trianglePoint1X, trianglePoint1Y, trianglePoint2X, trianglePoint2Y :: Double
trianglePoint3X, trianglePoint3Y :: Double
triangleRadius = 0.3
trianglePoint1X = 0.5
trianglePoint1Y = 0.5 - triangleRadius
trianglePoint2X = 0.5 - triangleRadius * cos (pi / 6)
trianglePoint2Y = 0.5 + triangleRadius * 0.5 {-sin (pi / 6)-}
trianglePoint3X = 0.5 + triangleRadius * cos (pi / 6)
trianglePoint3Y = 0.5 + triangleRadius * 0.5 {-sin (pi / 6)-}

markTrianglePoints :: [HPointF]
markTrianglePoints =
  [ HPointF trianglePoint1X trianglePoint1Y
  , HPointF trianglePoint2X trianglePoint2Y
  , HPointF trianglePoint3X trianglePoint3Y
  ]

markSquarePoints :: [HPointF]
markSquarePoints =
  [ HPointF 0.25 0.25
  , HPointF 0.75 0.25
  , HPointF 0.75 0.75
  , HPointF 0.25 0.75
  ]

markSelectedPoints :: [HPointF]
markSelectedPoints =
  [ HPointF 0.2 0.5
  , HPointF 0.5 0.2
  , HPointF 0.8 0.5
  , HPointF 0.5 0.8
  ]

-- | Draws a line between the given board points.  Expects the context to be
-- already set up to draw.
drawLine :: QPainter -> Coord -> Coord -> IO ()
drawLine painter
         (fromIntegral -> x0, fromIntegral -> y0)
         (fromIntegral -> x1, fromIntegral -> y1) =
  QPainter.drawLinePointF painter
    (HPointF (x0 + 0.5) (y0 + 0.5))
    (HPointF (x1 + 0.5) (y1 + 0.5))

-- | Draws an arrow from the first point to the second point.  Expects the
-- context to be already set up to draw.
drawArrow :: QPainter -> QPen -> QPen -> QBrush -> QPainterPath -> Coord -> Coord -> IO ()
drawArrow painter
          linePen
          arrowheadPen
          arrowheadBrush
          arrowheadPath
          (fromIntegral -> x0, fromIntegral -> y0)
          (fromIntegral -> x1, fromIntegral -> y1) = do
  -- Set up user space so that we can draw the line from (0,0) to
  -- (0,lineLength).
  let angle = atan ((y1 - y0) / (x1 - x0)) + if x0 <= x1 then 0 else pi
      len = sqrt ((y1 - y0)**2 + (x1 - x0)**2) - boardAnnotationArrowPullback
      tx = x0 + 0.5
      ty = y0 + 0.5
  QPainter.translateRaw painter tx ty
  QPainter.rotate painter (angle * radToDeg)

  QPainter.setPen painter linePen
  QPainter.drawLinePointF painter (HPointF 0 0) (HPointF len 0)

  QPainter.translateRaw painter len 0
  QPainter.setPen painter arrowheadPen
  QPainter.setBrush painter arrowheadBrush
  QPainter.drawPath painter arrowheadPath

  -- Undo all transformations.
  QPainter.translateRaw painter (-len) 0
  QPainter.rotate painter (-angle * radToDeg)
  QPainter.translateRaw painter (-tx) (-ty)

-- | Draws a dot to indicate that a variation is available where the given
-- player plays a stone here.  The dot is of the given color.
--
-- If @half@ is true, then only draws the bottom half of the stone, from 225deg
-- to 45deg, so as not to overlap with 'drawCurrent'.
drawVariation :: QPainter -> DrawingImplements -> Color -> Bool -> IO ()
drawVariation painter implements color half = do
  -- Use antialiasing for smooth stones.
  QPainter.setRenderHint painter QPainter.Antialiasing

  QPainter.setPen painter $ variationMarkerPen implements color
  QPainter.setBrush painter $ variationMarkerBrush implements color

  if half
    then let x = 0.5 - stoneVariationRadius
             w = 2 * stoneVariationRadius
             startAngle = 225
             spanAngle = 180
         in withScopedPtr QPainterPath.new $ \path -> do
           QPainterPath.arcMoveToRaw path x x w w startAngle
           QPainterPath.arcToRaw path x x w w startAngle spanAngle
           QPainter.drawPath painter path
    else QPainter.drawEllipsePointF painter
           (HPointF 0.5 0.5)
           stoneVariationRadius
           stoneVariationRadius

-- | Draws a dot to indicate that the current coordinate was played on in the
-- current node.
--
-- If @half@ is true, then only draws the top half of the stone, from 45deg to
-- 225deg, so as not to overlap with 'drawVariation'.
drawCurrent :: QPainter -> DrawingImplements -> Bool -> IO ()
drawCurrent painter implements half = do
  -- Use antialiasing for smooth stones.
  QPainter.setRenderHint painter QPainter.Antialiasing

  QPainter.setPen painter $ currentStoneMarkerPen implements
  QPainter.setBrush painter $ currentStoneMarkerBrush implements

  if half
    then let x = 0.5 - stoneVariationRadius
             w = 2 * stoneVariationRadius
             startAngle = 45
             spanAngle = 180
         in withScopedPtr QPainterPath.new $ \path -> do
           QPainterPath.arcMoveToRaw path x x w w startAngle
           QPainterPath.arcToRaw path x x w w startAngle spanAngle
           QPainter.drawPath painter path
    else QPainter.drawEllipsePointF painter
          (HPointF 0.5 0.5)
          currentStoneMarkerRadius
          currentStoneMarkerRadius

{-
-- | Draws a small filled arc centered on the current coordinate being drawn.
drawSmallDot :: Rgb -> Rgb -> Double -> Double -> Render ()
drawSmallDot fill border angle0 angle1 = do

  arc 0.5 0.5 stoneVariationRadius angle0 angle1
  setRgb fill
  fillPreserve
  setLineWidth stoneVariationBorderThickness
  setRgb border
  stroke

roundToPixels :: Double -> Double -> Render (Double, Double)
roundToPixels =
  (uncurry deviceToUser . mapTuple (fromIntegral . (round :: Double -> Int)) <=<) .
  userToDevice

type Rgb = (Double, Double, Double)

rgb :: Double -> Double -> Double -> Rgb
rgb = (,,)

rgb255 :: Double -> Double -> Double -> Rgb
rgb255 r g b = (r / 255, g / 255, b / 255)

setRgb :: Rgb -> Render ()
setRgb (r, g, b) = setSourceRGB r g b

pi_0_75, pi_1_75, pi_2 :: Floating a => a
pi_0_75 = pi * 0.75
pi_1_75 = pi * 1.75
pi_2 = pi * 2
-}

radToDeg :: Floating a => a
radToDeg = 180 / pi

type Rgb = HColor

rgb :: Int -> Int -> Int -> HColor
rgb r g b = HColor.Rgb r g b 255
