-- This file is part of Goatee.
--
-- Copyright 2014-2021 Bryan Gardiner
--
-- Goatee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Goatee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

-- | Implementation of the main window that contains the game board.
module Game.Goatee.Ui.Qt.MainWindow (
  MainWindow,
  create,
  destroy,
  display,
  myWindow,
  ) where

import Control.Monad (forM, forM_, join, liftM, unless)
import qualified Data.Foldable as F
import Data.Functor (void)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.List (intersperse)
import qualified Data.Map as Map
import Data.Maybe (catMaybes, fromMaybe)
import Foreign.Hoppy.Runtime (toCppEnum)
import Game.Goatee.Ui.Qt.Common
import qualified Game.Goatee.Ui.Qt.Actions as Actions
import Game.Goatee.Ui.Qt.Actions (Actions)
import qualified Game.Goatee.Ui.Qt.GamePropertiesPanel as GamePropertiesPanel
import Game.Goatee.Ui.Qt.GamePropertiesPanel (GamePropertiesPanel)
import qualified Game.Goatee.Ui.Qt.Goban as Goban
import Game.Goatee.Ui.Qt.Goban (Goban)
import qualified Game.Goatee.Ui.Qt.InfoLine as InfoLine
import Game.Goatee.Ui.Qt.InfoLine (InfoLine)
-- TODO(Qt) import qualified Game.Goatee.Ui.Qt.NodePropertiesPanel as NodePropertiesPanel
-- TODO(Qt) import Game.Goatee.Ui.Qt.NodePropertiesPanel (NodePropertiesPanel)
import qualified Game.Goatee.Ui.Qt.PlayPanel as PlayPanel
import Game.Goatee.Ui.Qt.PlayPanel (PlayPanel)
import qualified Graphics.UI.Qtah.Core.QEvent as QEvent
import qualified Graphics.UI.Qtah.Core.QObject as QObject
import qualified Graphics.UI.Qtah.Core.Types as Qt
import Graphics.UI.Qtah.Event (onEvent)
import qualified Graphics.UI.Qtah.Gui.QCloseEvent as QCloseEvent
import qualified Graphics.UI.Qtah.Gui.QKeyEvent as QKeyEvent
import qualified Graphics.UI.Qtah.Widgets.QAction as QAction
import qualified Graphics.UI.Qtah.Widgets.QBoxLayout as QBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QMainWindow as QMainWindow
import qualified Graphics.UI.Qtah.Widgets.QMenu as QMenu
import qualified Graphics.UI.Qtah.Widgets.QMenuBar as QMenuBar
import qualified Graphics.UI.Qtah.Widgets.QSplitter as QSplitter
import qualified Graphics.UI.Qtah.Widgets.QTabWidget as QTabWidget
import qualified Graphics.UI.Qtah.Widgets.QVBoxLayout as QVBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget

data MainWindow ui = MainWindow
  { myUi :: ui
  , myWindow :: QMainWindow.QMainWindow
  , myActions :: Actions ui
  , myInfoLine :: InfoLine ui
  , myGoban :: Goban ui
  , myPlayPanel :: PlayPanel ui
  , myGamePropertiesPanel :: GamePropertiesPanel ui
  -- TODO(Qt) , myNodePropertiesPanel :: NodePropertiesPanel ui
  , myDirtyChangedHandler :: IORef (Maybe Registration)
  , myFilePathChangedHandler :: IORef (Maybe Registration)
  }

create :: UiCtrl go ui => ui -> IO (MainWindow ui)
create ui = do
  window <- QMainWindow.new
  QWidget.resizeRaw window 640 480
  provideQMainWindow ui window

  actions <- Actions.create ui

  widget <- QWidget.new
  box <- QVBoxLayout.new
  QWidget.setLayout widget box
  QMainWindow.setCentralWidget window widget

  menuBar <- QMenuBar.new
  QMainWindow.setMenuBar window menuBar

  menuFile <- QMenuBar.addNewMenu menuBar "&File"
  menuFileNew <- QMenu.addNewMenu menuFile "&New file"
  addActionsToMenu menuFileNew actions
    [ Actions.myFileNew9Action
    , Actions.myFileNew13Action
    , Actions.myFileNew19Action
    , Actions.myFileNewCustomAction
    ]

  addActionsToMenu menuFile actions
    [ Actions.myFileOpenAction
    , Actions.myFileSaveAction
    , Actions.myFileSaveAsAction
    ]
  QMenu.addSeparator menuFile
  addActionsToMenu menuFile actions
    [ Actions.myFileCloseAction
    , Actions.myFileQuitAction
    ]

  menuEdit <- QMenuBar.addNewMenu menuBar "&Edit"
  addActionsToMenu menuEdit actions
    [ Actions.myEditCutNodeAction
    , Actions.myEditCopyNodeAction
    , Actions.myEditPasteNodeAction
    ]

  menuGame <- QMenuBar.addNewMenu menuBar "&Game"
  addActionsToMenu menuGame actions
    [ Actions.myGamePassAction
    ]

  menuGameVariations <- QMenu.addNewMenu menuGame "&Variations"

  QMenu.addAction menuGameVariations $ Actions.myGameVariationsChildAction actions
  QMenu.addAction menuGameVariations $ Actions.myGameVariationsCurrentAction actions
  QMenu.addSeparator menuGameVariations
  QMenu.addAction menuGameVariations $ Actions.myGameVariationsBoardMarkupOnAction actions
  QMenu.addAction menuGameVariations $ Actions.myGameVariationsBoardMarkupOffAction actions

  menuTool <- QMenuBar.addNewMenu menuBar "&Tool"

  -- TODO Toolbar (or remove).
  --toolbar <- toolbarNew
  --boxPackStart boardBox toolbar PackNatural 0
  --toolbarSetStyle toolbar ToolbarText

  let addToolSeparator = void $ QMenu.addSeparator menuTool
      addTool (AnyTool tool) = do
        let action =
              fromMaybe (error $ "No action for tool with type: " ++ show (toolType tool)) $
              Map.lookup (toolType tool) (Actions.myToolActionMap actions)
        QMenu.addAction menuTool action
    in join $ fmap (sequence_ . intersperse addToolSeparator . catMaybes) $
       forM toolOrdering $ \toolGroup -> do
         tools <- filter (\(AnyTool tool) -> toolIsImplemented tool) <$>
                  mapM (findTool ui) toolGroup
         return $ if null tools
                  then Nothing
                  else Just $ mapM_ addTool tools

  menuView <- QMenuBar.addNewMenu menuBar "&View"
  QMenu.addAction menuView $ Actions.myViewHighlightCurrentMovesAction actions

  menuViewStones <- QMenu.addNewMenu menuView "&Stones"
  addActionsToMenu menuViewStones actions
    [ Actions.myViewStonesRegularModeAction
    , Actions.myViewStonesOneColorModeAction
    , Actions.myViewStonesBlindModeAction
    ]

  menuHelp <- QMenuBar.addNewMenu menuBar "&Help"
  addActionsToMenu menuHelp actions
    [ Actions.myHelpKeyBindingsAction
    , Actions.myHelpAboutAction
    ]

  infoLine <- InfoLine.create ui
  QBoxLayout.addWidgetWithStretchAndAlignment box (InfoLine.myWidget infoLine) 0
    Qt.AlignHCenter

  splitter <- QSplitter.new
  QBoxLayout.addWidgetWithStretch box splitter 1

  -- TODO Set the splitter's initial position.
  --panedSetPosition hPaned 400 -- (truncate (fromIntegral hPanedMax * 0.8))

  goban <- Goban.create ui
  QSplitter.addWidget splitter $ Goban.myWidget goban

  controlsTabs <- QTabWidget.new
  QWidget.setMinimumSizeRaw controlsTabs 100 0
  QSplitter.addWidget splitter controlsTabs

  playPanel <- PlayPanel.create ui
  gamePropertiesPanel <- GamePropertiesPanel.create ui
  -- TODO(Qt) nodePropertiesPanel <- NodePropertiesPanel.create ui
  QTabWidget.addTab controlsTabs (PlayPanel.myWidget playPanel) "Play"
  QTabWidget.addTab controlsTabs (GamePropertiesPanel.myWidget gamePropertiesPanel) "Game"
  -- TODO(Qt) QTabWidget.addTab controlsTabs (NodePropertiesPanel.myWidget nodePropertiesPanel) "Properties"

  dirtyChangedHandler <- newIORef Nothing
  filePathChangedHandler <- newIORef Nothing

  let me = MainWindow { myUi = ui
                      , myWindow = window
                      , myActions = actions
                      , myInfoLine = infoLine
                      , myGoban = goban
                      , myPlayPanel = playPanel
                      , myGamePropertiesPanel = gamePropertiesPanel
                      -- TODO(Qt) , myNodePropertiesPanel = nodePropertiesPanel
                      , myDirtyChangedHandler = dirtyChangedHandler
                      , myFilePathChangedHandler = filePathChangedHandler
                      }

  initialize me

  onEvent window $ \(keyEvent :: QKeyEvent.QKeyEvent) -> do
    key <- toCppEnum . fromIntegral <$> QKeyEvent.key keyEvent
    mods <- QKeyEvent.modifiers keyEvent
    case undefined of
      -- Escape key with no modifiers: focus the goban.
      _ | key == Qt.KeyEscape && mods == Qt.noModifier -> do
        QWidget.setFocus $ Goban.myWidget goban
        return True
      _ -> return False

  onEvent window $ \(evt :: QCloseEvent.QCloseEvent) -> do
    close <- fileClose ui
    -- Need to explicitly ignore this event to prevent the window from closing.
    unless close $ QEvent.ignore evt
    return True

  QWidget.setFocus $ Goban.myWidget goban

  return me

-- | Initialization that must be done after the 'UiCtrl' is available.
initialize :: UiCtrl go ui => MainWindow ui -> IO ()
initialize me = do
  let ui = myUi me

  writeIORef (myDirtyChangedHandler me) =<<
    liftM Just (registerDirtyChangedHandler ui "MainWindow" False $ \_ -> updateWindowTitle me)
  writeIORef (myFilePathChangedHandler me) =<<
    liftM Just (registerFilePathChangedHandler ui "MainWindow" True $ \_ _ -> updateWindowTitle me)

destroy :: UiCtrl go ui => MainWindow ui -> IO ()
destroy me = do
  Actions.destroy $ myActions me
  InfoLine.destroy $ myInfoLine me
  Goban.destroy $ myGoban me
  PlayPanel.destroy $ myPlayPanel me
  GamePropertiesPanel.destroy $ myGamePropertiesPanel me
  -- TODO(Qt) NodePropertiesPanel.destroy $ myNodePropertiesPanel me

  let ui = myUi me
  F.mapM_ (unregisterDirtyChangedHandler ui) =<< readIORef (myDirtyChangedHandler me)
  F.mapM_ (unregisterFilePathChangedHandler ui) =<< readIORef (myFilePathChangedHandler me)

  -- TODO Is this correct?
  QObject.deleteLater $ myWindow me

-- | Makes a 'MainWindow' visible.
display :: MainWindow ui -> IO ()
display = QWidget.show . myWindow

-- | Takes a object of generic type, extracts a bunch of actions from it, and
-- adds those actions to a menu.
addActionsToMenu :: QMenu.QMenu -> a -> [a -> QAction.QAction] -> IO ()
addActionsToMenu menu actions accessors =
  forM_ accessors $ \accessor -> QMenu.addAction menu $ accessor actions

updateWindowTitle :: UiCtrl go ui => MainWindow ui -> IO ()
updateWindowTitle me = do
  let ui = myUi me
  fileName <- getFileName ui
  dirty <- getDirty ui
  let title = fileName ++ " - Goatee"
      addDirty = if dirty then ('*':) else id
  QWidget.setWindowTitle (myWindow me) $ addDirty title
