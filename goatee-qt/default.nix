# This file is part of Goatee.
#
# Copyright 2014-2021 Bryan Gardiner
#
# Goatee is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Goatee is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Goatee.  If not, see <http://www.gnu.org/licenses/>.

{ mkDerivation, base, containers, directory, filepath, goatee
, hoppy-runtime, HUnit, lib, mtl, parsec, qt5
, qtah ? null, qtah-qt5 ? null
}:
let
  qtah' = if qtah != null then qtah else qtah-qt5;
in
mkDerivation {
  pname = "goatee-qt";
  version = "0.4.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  enableSeparateDataOutput = true;
  libraryHaskellDepends = [
    base containers directory filepath goatee hoppy-runtime mtl parsec qtah'
  ];
  executableHaskellDepends = [ base hoppy-runtime qtah' ];
  testHaskellDepends = [ base HUnit ];
  homepage = "https://khumba.net/projects/goatee";
  description = "A monadic take on a 2,500-year-old board game - Qt UI";
  license = lib.licenses.agpl3Plus;

  # The build is broken for profiling; it can't find the Prelude...
  enableLibraryProfiling = false;
  enableSharedExecutables = true;

  buildTools = [ qt5.wrapQtAppsHook ];
}
